implementation module Builtin.Syntax

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdArray
import StdFunctions
import StdList
import StdString

import Data.Error
from Data.Func import $
import Text

import Regex

import Cloogle.API
import Cloogle.DB

import Builtin.Predef

exact s = ExactPattern s
match s = RegexPattern (fromOk (compileRegex s))
fullMatch s = match (concat3 "^" s "$")

builtin_syntax :: [SyntaxEntry]
builtin_syntax =
	[ bs_basicvalues
	, bs_case
	, bs_class
	, bs_code
	, bs_comments
	, bs_context
	, bs_define_constant
	, bs_define_graph
	, bs_dotdot
	, bs_exists
	, bs_extensible_adt
	, bs_forall
	, bs_foreign
	, bs_funcdep
	, bs_function_definition
	, bs_guard
	, bs_generic
	, bs_hierarchical_modules
	, bs_import
	, bs_infix
	, bs_instance
	, bs_lambda
	, bs_layout_rule
	, bs_let
	, bs_let_before
	, bs_list_expressions
	, bs_macro
	, bs_module
	, bs_newtype
	, bs_newtype_abstract
	, bs_overloaded_maybe_constructors
	, bs_overloaded_type_variable
	, bs_pattern_named
	, bs_pattern_predicate
	, bs_qualified_identifier
	, bs_record_disambiguation
	, bs_selection_array
	, bs_selection_array_unique
	, bs_selection_record
	, bs_selection_record_unique
	, bs_strict
	, bs_synonym
	, bs_synonym_abstract
	, bs_type_definition
	, bs_type_specification
	, bs_unique
	, bs_update_array
	, bs_update_record
	, bs_where_class
	, bs_where_instance
	, bs_where_local
	, bs_with
	, bs_with_compiler_settings
	, bs_zf
	]

EX :: !String -> SyntaxExample 
EX c = {example = c, cleanjs_start = ?None, bootstrap = [], requires_itask_compiler = False}
EXs :: !String !String -> SyntaxExample
EXs s c = {EX c & cleanjs_start = ?Just s}

add_imports :: ![String] !SyntaxExample -> SyntaxExample
add_imports is se = {se & bootstrap=["import " +++ mod \\ mod <- is] ++ se.bootstrap}

add_bootstrap :: !String !SyntaxExample -> SyntaxExample
add_bootstrap bs se = {se & bootstrap=se.bootstrap ++ [bs]}

requires_itask_compiler :: !SyntaxExample -> SyntaxExample
requires_itask_compiler se = {se & requires_itask_compiler=True}

bs_basicvalues =
	{ syntax_title         = "basic values"
	, syntax_patterns      = [exact "E":map fullMatch ["'.'", "[+-]?\\d+", "[+-]?0[0-7]+", "[-+]?0x[0-9a-fA-F]+"]]
	, syntax_code          = ["0x...", "0...", "'...'", "...E..."]
	, syntax_description   = join "\n"
		[ "Constant basic values can be of type {{`Int`}}, {{`Char`}} and {{`Real`}}. See also {{`Bool`}}."
		, ""
		, "Integers can be specified in decimal (default), octal (`0` prefix) or hexadecimal (`0x` prefix) notation."
		, ""
		, "Characters can either be printable characters (except `'`) or an escape sequence."
		, "An escape sequence is a character escape, a hexademical escape (starting with `x`), an octal escape (starting with `0` to `7`) or a decimal escape (starting with `d`)."
		, ""
		, "Reals can be suffixed by a power of ten in the scientific notation."
		, ""
		, "Basic values can also be pattern matched with these notations."
		]
	, syntax_doc_locations = [CLR "4.1.1"]
	, syntax_examples      = map (EXs "rhs")
		[ "(42, -42, +42)          // Tuple with 42, -42 and 42 in decimal notation"
		, "(052, -052, +052)       // Tuple with 42, -42 and 42 in octal notation"
		, "(0x2a, -0x2a, +0x2A)    // Tuple with 42, -42 and 42 in hexadecimal notation"
		, "('a', '\\x2a', '\\052')   // Tuple with a normal character and twice the character with ASCII value 42"
		, "['\\n', '\\r', '\\f', '\\b', '\\t', '\\v', '\\\\', '\\'', '\\\"']\n" +
		  "                        // All character escapes"
		, "(42.0, -42.0, 42E-10, +42.0E+10, -42.0E10)\n" +
		  "                        // Several reals"
		]
	}

bs_case =
	{ syntax_title         = "case expression"
	, syntax_patterns      = map exact ["case", "of", "case of", "->", "="]
	, syntax_code          = ["case ... of ..."]
	, syntax_description   = join "\n"
		[ "Pattern match on an expression and do something depending on the alternative of the matching pattern."
		, "Both `->` and `=` can be used to separate patterns and alternatives, however, they cannot be mixed."
		]
	, syntax_doc_locations = [CLR "3.4.2"]
	, syntax_examples      =
		[ EXs "macro"
		  "isJust m = case m of\n\t?Just _ -> True\n\t_       -> False"
		]
	}

bs_class =
	{ syntax_title         = "class"
	, syntax_patterns      = map exact ["class"]
	, syntax_code          =
		[ "class ... ... :: ..."
		, "class ... ... where ..."
		]
	, syntax_description   = join "\n"
		[ "Classes are (sets of) overloaded functions. For classes with only one member function, a simplified syntax exists."
		, ""
		, "Types can instantiate classes with the {{`instance`}} keyword."
		]
	, syntax_doc_locations = [CLR "6.1"]
	, syntax_examples      =
		[ EX  "class zero a :: a                   // one member"
		, EX $ join "\n"
			[ "class Text s                        // multiple members"
			, "where"
			, "\ttextSize :: !s -> Int"
			, "\tconcat :: ![s] -> s"
			, "\t/* etc... */"
			]
		, add_imports ["StdOverloaded"] $ EX $ join "\n"
			[ "class sort1 l :: (l a) -> l a | < a // higher-kinded class; instances do not have access to `a`"
			, "class sort2 (l a) :: (l a) -> l a   // higher-kinded class; instances have access to `a`"
			]
		]
	}

bs_code =
	{ syntax_title         = "ABC code"
	, syntax_patterns      = map exact ["code", "inline", "code inline"]
	, syntax_code          = ["... = code [inline] { ... }"]
	, syntax_description   = join "\n"
		[ "A code block with raw ABC instructions, which can be used for primitive functions like integer addition, for linking with C, bypassing the type system... welcome down the rabbit hole!"
		, ""
		, "When `inline` is used, the function will be inlined when applied in a strict context."
		]
	, syntax_doc_locations = [CLR "11.2"]
	, syntax_examples      = map EX
		[ "add :: !Int !Int -> Int                   // Primitive function\nadd a b = code inline {\n\taddI\n}"
		, "sleep :: !Int !*World -> *(!Int, !*World) // Linking with C\nsleep n w = code {\n\tccall sleep \"I:I:A\"\n}"
		, "cast :: !.a -> .b                         // Bypassing the type system\ncast _ = code {\n\tno_op\n}"
		]
	}

bs_comments =
	{ syntax_title         = "comments"
	, syntax_patterns      = [exact "//", match "^/\\*", exact "*/"]
	, syntax_code          = ["// ...", "/* ... */"]
	, syntax_description   = "`//` adds a single-line comment. `/*` and `*/` encapsulate a multi-line comment. Multi-line comments can be nested."
	, syntax_doc_locations = [CLR "B.2"]
	, syntax_examples      = map EX
		[ "Start = 37 // Single-line comment"
		, "Start = /* inline or multi-line comment */ 37"
		]
	}

bs_context =
	{ syntax_title         = "type context"
	, syntax_patterns      = map exact ["|", "&", ",", "special"]
	, syntax_code          = [":: ... | ..., ... [special ...=...]", "| ... & ..., ..."]
	, syntax_description   = join "\n"
		[ "A type context indicates what {{class}}es must be instantiated by type variables."
		, ""
		, "For function types, the type context starts with `|`."
		, "Several classes can be given to the same variable with `,`."
		, "To add multiple restrictions, use `&`."
		, ""
		, "In constructors, the type context starts with `&`."
		, ""
		, "Uniqueness constraints can be given with `,`. For details, see under {{`,`}}."
		, ""
		, "With the `special` keyword, specialised instances for certain type instantiations are exported for efficiency."
		, ""
		, "The context of a generic function can only contain other generic functions."
		, "The generic context is written without kinds and separated by `,` as seen in the example."
		]
	, syntax_doc_locations = [CLR "6.2"]
	, syntax_examples      = map (add_imports ["StdEnv","Data.GenEq"] o EX)
		[ "add :: a a -> a | + a              // a must instantiate +\nadd x y = x + y"
		, "sum :: [a] -> a | zero, + a        // a must instantiate zero and +\nsum []     = zero\nsum [x:xs] = x + sum xs"
		, "(<+) infixr 5 :: a b -> String\n" +
		  "\t| toString a & toString b      // a and b must instantiate toString\n" +
		  "(<+) x y = toString x +++ toString y"
		, "contains :: a [a] -> Bool special a=Int // specialised instance for integer lists for efficiency"
		, "generic gFun a | gEq a :: a -> Int // generic context"
		]
	}

bs_define_constant =
	{ syntax_title         = "graph definition"
	, syntax_patterns      = map exact ["=:"]
	, syntax_code          = ["... =: ..."]
	, syntax_description   = join "\n"
		[ "Defining constants with `=:` at the top level makes sure they are shared through out the program; hence, they are evaluated only once."
		, ""
		, "This is the default understanding of `=` in local scope."
		, ""
		, "The inverse is {{`=>`}}, which defines an identifier to be a constant function."
		]
	, syntax_doc_locations = [CLR "3.6"]
	, syntax_examples      = [add_imports ["StdEnum"] $ EXs "macro" "mylist =: [1..10000]"]
	}
bs_define_graph =
	{ syntax_title         = "constant function definition"
	, syntax_patterns      = map exact ["=>"]
	, syntax_code          = ["... => ..."]
	, syntax_description   = join "\n"
		[ "Defining constants with `=>` makes sure they are interpreted as constant functions; hence, they are evaluated every time they are needed."
		, ""
		, "This is the default understanding of `=` in global scope."
		, ""
		, "The inverse is {{`=:`}}, which defines an identifier to be a graph."
		]
	, syntax_doc_locations = [CLR "3.6"]
	, syntax_examples      = [add_imports ["StdEnum"] $ EXs "macro" "mylist => [1..10000]"]
	}

bs_dotdot =
	{ syntax_title         = "dotdot expression"
	, syntax_patterns      = map exact ["dotdot", "dot-dot", ".."] ++ map fullMatch ["\\[\\w\\.\\.\\]", "\\[\\w\\.\\.\\w\\]", "\\[\\w,\\w\\.\\.\\]", "\\[\\w,\\w\\.\\.\\w\\]"]
	, syntax_code          = ["[i..]", "[i..k]", "[i,j..]", "[i,j..k]"]
	, syntax_description   = join "\n"
		[ "A shorthand for lists of enumerable types."
		, ""
		, "To use these expressions, you must import {{`StdEnum`}}. The underlying functions are defined in {{`_SystemEnum`}}."
		]
	, syntax_doc_locations = [CLR "4.2.1"]
	, syntax_examples      = map (add_imports ["StdEnum"] o EXs "macro")
		[ "xs = [0..]     // 0, 1, 2, 3, ..."
		, "xs = [0,2..]   // 0, 2, 4, 6, ..."
		, "xs = [0..10]   // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
		, "xs = [0,2..10] // 0, 2, 4, 6, 8, 10"
		]
	}

bs_exists =
	{ syntax_title         = "existential quantifier"
	, syntax_patterns      = [exact "E", match "^E\\.", match "^\\^$"]
	, syntax_code          =
		[ ":: ... = E. ...: ... [& ...]"
		, "... :: E.^...: ..."
		]
	, syntax_description   = join "\n"
		[ "Existential quantifiers make it possible to define types that can hold expressions of different types, without having the type of the 'inner' expressions reflected in the 'outer' type."
		, "For example, it is possible to define a polymorphic list type that can hold elements of different types."
		, "It is optionally possible to add a type constraint to the existentially quantified type."
		, ""
		, "In local function definitions, type variables can be existentially quantified if the type of the local definition depends on the type of an overloaded or polymorphic lifted argument."
		, "Such type variables must be preceded by `^`, e.g.: `E.^a: a -> a`."
		]
	, syntax_doc_locations = [CLR "5.1.3"]
	, syntax_examples      =
		[ EX $ join "\n"
			[ ":: List = E.e: Cons e List | Nil"
			, "Start = Cons 5 (Cons 'a' (Cons \"abc\" Nil))"
			]
		, add_imports ["StdOverloaded"] $ EX $ join "\n"
			[ ":: ToStringList = E.e: Cons e ToStringList & toString e | Nil"
			, "print :: ToStringList -> [String]"
			, "print (Cons x xs) = [toString x:print xs]"
			, "print Nil = []"
			]
		, EX $ join "\n"
			[ "f :: a -> a"
			, "f x = y"
			, "where"
			, "\ty :: E.^a: a"
			, "\ty = x"
			]
		]
	}

bs_extensible_adt =
	{ syntax_title         = "extensible algebraic data type"
	, syntax_patterns      = map exact ["..", "|"]
	, syntax_code          = [":: T | ...", ":: T = ... | ... | .."]
	, syntax_description   = join "\n"
		[ "Extensible algebraic data types are ADTs that can be extended in other modules."
		, "One module can declare the ADT as extendible by adding the `..` constructor."
		, "Other modules can then extend it."
		, "It is not possible to derive generic functions for EADTs."
		]
	, syntax_doc_locations = [CLR "5.1.5"]
	, syntax_examples      =
		[ EX
		  ":: T = ..      // Declare T as an EADT"
		, EX
		  ":: T = C1 | .. // Declare T to be an EADT with at least the constructor C1"
		, add_bootstrap ":: T = .." $ EX
		  ":: T | C       // Extend the EADT T with constructor C"
		]
	}

bs_forall =
	{ syntax_title         = "universal quantifier"
	, syntax_patterns      = [exact "A", match "^A."]
	, syntax_code          = ["A. ...:"]
	, syntax_description   = "Explicitly marks polymorphic type variables. Clean does not yet allow universal quantifiers on the topmost level."
	, syntax_doc_locations = [CLR "3.7.4"]
	, syntax_examples      = map (add_imports ["StdEnv"] o EX)
		[ "h :: (A.a: [a] -> Int) -> Int // The quantifier is needed to apply the function to both a [Int] and a [Char]\nh f = f [1..100] + f ['a'..'z']"
		, ":: T = C (A.a: a -> a)        // In a type"
		, "hd :: A.a: [a] -> a           // Not allowed: A. on the topmost level"
		]
	}

bs_foreign =
	{ syntax_title         = "foreign export"
	, syntax_patterns      = map exact ["foreign", "export", "ccall", "stdcall"]
	, syntax_code          =
		[ "foreign export [ccall | stdcall] ..."
		]
	, syntax_description   = join "\n"
		[ "Exports the Clean function symbol to the binary."
		, ""
		, "This is only possible if the function is also in the definition module and all arguments are basic types or tuples and fully strict."
		]
	, syntax_doc_locations = [CLR "11.1"]
	, syntax_examples      = map EX
		[ "foreign export factorial         // Export the factorial function"
		, "foreign export stdcall factorial // Idem but with the stdcall calling convention"
		]
	}

bs_funcdep =
	{ syntax_title         = "functional dependency"
	, syntax_patterns      = map exact ["~"]
	, syntax_code          =
		[ "class ... ~... ..."
		]
	, syntax_description   = "Lets you point the type checker to the type that is determined by the other types (undocumented and experimental)."
	, syntax_doc_locations = []
	, syntax_examples      = map (add_imports ["StdEnv"] o requires_itask_compiler o EX)
		[ "class plus a b ~c :: a b -> c // c is determined by a and b"
		]
	}

bs_function_definition =
	{ syntax_title         = "function definition"
	, syntax_patterns      = map exact ["="]
	, syntax_code          = ["... = ..."]
	, syntax_description   = join "\n"
		[ "Specifies the implementation of a function."
		, ""
		, "Instead of `=`, also {{`=:`}} and {{`=>`}} may be used to separate the pattern from the right-hand side."
		, "These have different semantics."
		]
	, syntax_doc_locations = [CLR "3"]
	, syntax_examples      = map EX
		[ "map :: (a -> b) [a] -> [b]\nmap f []     = []\nmap f [x:xs] = [f x:map f xs]"
		]
	}

bs_guard =
	{ syntax_title         = "guard"
	, syntax_patterns      = map exact ["|", "=", "otherwise"]
	, syntax_code          = ["| ... = ...", "| otherwise = ..."]
	, syntax_description   = join "\n"
		[ "Guards let you specify function alternatives with boolean expressions."
		, "A final `otherwise` guard can be used to ensure the function is total."
		, ""
		, "Guards can be nested with indentation."
		, "However, only the toplevel guards may be partial."
		, ""
		, "To separate the guard from the alternative, both {{`=`}} and {{`=>`}} may be used (with different semantics; see {{`=>`}})."
		, "However, one must be consistent with this throughout the function."
		]
	, syntax_doc_locations = [CLR "3.3"]
	, syntax_examples      = map (add_imports ["StdEnv"] o EX)
		[ "sign :: !Int -> Int\nsign n\n| n  < 0    = -1 // Negative number\n| n == 0    =  0 // Zero\n| otherwise =  1 // Must be positive"
		]
	}

bs_generic =
	{ syntax_title         = "generic function definition"
	, syntax_patterns      = map exact ["generic", "derive", "of", "with", "*!", "\\", "class"] ++ [fullMatch "\\{\\|.*\\|\\}"]
	, syntax_code          =
		[ "generic ... ... [| ...] [*!] :: ... "
		, "derive ... ... [of ...] [with ...]"
		, "derive class ...  [\\ ...] ..."
		, "instance ... ... derive ..."
		]
	, syntax_description   = join "\n"
		[ "With generics, a function can be defined once and derived for (almost) all possible types."
		, "This avoids very similar code snippets."
		, "Generic functions are said to be kind-indexed, i.e., a generic is actually a group of functions with different types depending on the kind of the deriving type."
		, ""
		, "Like classes, generic functions can appear in the context of other classes (see section 6.9 of the language report)."
		, "The iTasks compiler allows deriving all generics in the class with one statement, `derive class`."
		, "It is also possible to exclude some generic functions with `derive class ... \\ ...`."
		, ""
		, "It is also possible to derive a regular class instance using a generic function."
		, "This is not fully documented yet; see https://gitlab.com/cloogle/cloogle-web/-/issues/243 for details."
		]
	, syntax_doc_locations = [CLR "7.2"]
	, syntax_examples      =
		[ add_imports ["StdEnv","StdGeneric"] $ EX $
		  "generic gEq a :: !a !a -> Bool                    // The type of a generic function\n" +
		  "gEq{|Int|} x y = x == y                           // Implementation of a generic\n" +
		  "gEq{|PAIR|} fx fy (PAIR x1 y1) (PAIR x2 y2) = fx x1 x2 && fy y1 y2"
		, add_imports ["Data.GenEq"] $ EX
		  "derive gEq (,,,,,,,,)                             // Deriving the gEq generic for 9-tuples"
		, add_imports ["StdGeneric"] $ add_bootstrap "generic gConsName a :: a -> String" $ EX
		  "gConsName{|CONS of d|} _ _ = d.gcd_name           // Using type information"
		, add_imports ["StdGeneric"] $ add_bootstrap "generic gConsName a :: a -> String" $ EX
		  "gConsName{|CONS of {gcd_name}|} _ _ = gcd_name    // Using a specific field of type information, the compiler can optimise the function"
		, add_imports ["StdGeneric", "StdMisc", "Data.GenDefault"] $ EX $
		  "generic gFun a | gDefault a :: a -> Int           // A generic function with a generic context. The context will become an argument.\n" +
		  "derive gFun CONS of {gcd_arity} with f _          // A derivation that does not use all arguments. The compiler can optimize even more.\n" +
		  "gFun{|CONS of {gcd_arity}|} f _ _ = undef         // A derivation that does not use the context and only one field of the generic type descriptor, the compiler can optimize for this."
		, add_imports ["StdGeneric"] $ requires_itask_compiler $ EX $
		  "generic gShared a *! :: a                         // A generic function using a bimap without uniqueness attributes (requires the iTask compiler)\n"
		, add_imports ["StdGeneric", "Text.GenJSON"] $ add_bootstrap ":: T1 = T1\n:: T2 = T2" $ requires_itask_compiler $ EX $
		  "class json a | JSONEncode{|*|}, JSONDecode{|*|} a // Define a generic class collection containing three generic functions.\n" +
		  "derive class json T1                              // Derive all three generic functions with one derive statement.\n" +
		  "derive class json \\ JSONDecode T2                 // Derive all functions from gc but exempt JSONDecode (useful if you want to specialise just that function)."
		]
	}

bs_hierarchical_modules =
	{ syntax_title         = "hierarchical module names"
	, syntax_patterns      = [exact "."]
	, syntax_code          = ["... . ..."]
	, syntax_description   = "Modules can be structured hierarchically. For instance, module `Control.Monad` can be found in `Control/Monad.[di]cl`."
	, syntax_doc_locations = [CLR "D.1.3"]
	, syntax_examples      = [EX "definition module Control.Monad"]
	}

bs_import =
	{ syntax_title         = "imports"
	, syntax_patterns      = map exact ["import", "from", "qualified", "as", "=>", "code", "library", "()", ".."]
	, syntax_code          =
		[ "import [qualified] ... [as ...]"
		, "from ... import [qualified] ..."
		, "import ... => qualified ..."
		, "import code from [library] ..."
		]
	, syntax_description   = join "\n"
		[ "Imports code from other modules."
		, ""
		, "With the `from` keyword, one can achieve more granularity."
		, ""
		, "In case of name clashes, `qualified` can be used (undocumented)."
		, ""
		, "Moreover, you can import from object files, windows DLLs and Unix shared and static library files."
		, "The libraries are provided to the linker in the same order as provided in the Clean module, while static libraries are provided before shared ones."
		]
	, syntax_doc_locations = [CLR "2.5"]
	, syntax_examples      = map EX
		[ "import StdEnv                          // Import all code from the StdEnv definition module."
		, "from StdFunc import o                  // Import only the o function from StdFunc."
		, "import qualified Data.Map              // Import Data.Map such that functions are available as e.g. 'Data.Map'.get."
		, "import code from \"tty.\"                // Import functions from the object file matching 'Clean System Files/tty.*'"
		, "import code from library \"msvcrt\"      // Import functions from a linked DLL according to the msvcrt file in Clean System Files.\n" +
		  "                                       // The file should start with the DLL name (e.g. msvcrt.dll) and followed by one line per function you want to link.\n" +
		  "                                       // On non-Windows systems this import is ignored."
		, "import code from library \"-lsqlite3\"   // Import functions from the shared library sqlite3.\n" +
		  "                                       // This import is ignored on Windows systems."
		] ++ map (add_imports ["StdEnv"] o EX)
		[ "from Data.Map import :: Map            // Import only the type Map."
		, "from Data.Map import :: Map(..)        // Import the type Map and all constructors."
		, "from Data.Map import :: Map(Bin)       // Import the type Map and the Bin constructor."
		, "from Data.Foldable import class Foldable(foldr1()) // Import the class Foldable and the foldr1 member, without its default implementation if it has one."
		, "from StdList import qualified foldr    // Import foldr as 'StdList'.foldr, and nothing else."
		] ++ map (requires_itask_compiler o EX)
		[ "import qualified Data.Map as M         // Import Data.Map to use as e.g. 'M'.get (only supported by the iTask compiler)."
		, "import Control.Monad => qualified join // Import Control.Monad except for join; join is imported qualified (only supported by the iTask compiler, and only in implementation modules)."
		]
	}

bs_infix =
	{ syntax_title         = "infix operator"
	, syntax_patterns      = map exact ["infix", "infixl", "infixr"]
	, syntax_code          = ["infix[l,r] [...]"]
	, syntax_description   = join "\n"
		[ "Defines a function with arity 2 that can be used in infix position."
		, ""
		, "The following number, if any, determines the precedence."
		, ""
		, "`infixl` and `infixr` indicate associativity."
		]
	, syntax_doc_locations = [CLR "3.7.2"]
	, syntax_examples      =
		[ EX          "(bitor) infixl 6 :: !Int !Int -> Int // Left-associative infix function with precedence 6"
		, EXs "macro" "(o) infixr 9                         // Infix macro\n(o) f g :== \\x -> f (g x)"
		, EX          ":: MyType = (:+:) infixl 6 Int Int   // Infix data constructor, can be used as (5 :+: 10)"
		]
	}

bs_instance =
	{ syntax_title         = "instance"
	, syntax_patterns      = map exact ["instance"]
	, syntax_code          = ["instance ... ... where ..."]
	, syntax_description   = "Defines an instantiation of a {{class}} for a type."
	, syntax_doc_locations = [CLR "6.1"]
	, syntax_examples      = map (add_imports ["StdOverloaded"] o EX)
		[ "instance zero Int\nwhere\n\tzero = 0"
		, "instance zero Real\nwhere\n\tzero = 0.0"
		]
	}

bs_lambda =
	{ syntax_title         = "lambda abstraction"
	, syntax_patterns      = map exact ["lambda", "=", "->", "."]
	, syntax_code          = ["\\... -> ...", "\\... . ...", "\\... = ..."]
	, syntax_description   = "An anonymous, inline function."
	, syntax_doc_locations = [CLR "3.4.1"]
	, syntax_examples      = map (add_imports ["StdList"] o EXs "macro")
		[ "(o) f g = \\x -> f (g x)         // Simple lambda expression"
		, "swapall = map (\\(x,y) -> (y,x)) // Pattern matching in lambda arguments"
		, "mul     = \\x y -> x * y         // Multiple arguments (of course, it would be better to write `mul x y = x * y` or `mul = (*)`)"
		]
	}

bs_layout_rule =
	{ syntax_title         = "layout rule"
	, syntax_patterns      = map exact [";", "{", "}"]
	, syntax_code          = ["...;", "{ ... }"]
	, syntax_description   =
		"Most Clean programs are written using the layout rule, which means that scopes are indicated with indent levels." +
		"The layout sensitive mode can be turned off by adding a semicolon `;` at the end of the {{module}} line." +
		"Then, scopes have to be indicated with `{ ... }` and definitions have to end with `;`."
	, syntax_doc_locations = [CLR "2.3.3"]
	, syntax_examples      = [EX $
		"module test;\n" +
		"import StdEnv;\n" +
		"Start :: [(Int,Int)];\n" +
		"Start = [(x,y) \\\\ x <- odds, y <- evens];\n" +
		"where\n" +
		"{\n" +
		"\todds  = [1,3..9];\n" +
		"\tevens = [0,2..8];\n" +
		"}"]
	}

bs_let =
	{ syntax_title         = "let expression"
	, syntax_patterns      = map exact ["let", "in", "let in"]
	, syntax_code          = ["let ... in ..."]
	, syntax_description   = "An expression that introduces new scope."
	, syntax_doc_locations = [CLR "3.5.1"]
	, syntax_examples      =
		[ add_imports ["StdEnv"] $
		  EXs "macro"
		  "fac n = let fs = [1:1:[(fs!!(i-1)) + (fs!!(i-2)) \\\\ i <- [2..]]] in fs !! n"
		, add_imports ["StdMisc"] $
		  add_bootstrap "body = undef" $
		  add_bootstrap "expr = undef" $
		  add_bootstrap "expression = undef" $
		  EXs "rhs"
		  "let // Multi-line let expressions\n\tfunction args = body\n\tselector = expr\n\t// ...\nin expression"
		]
	}
bs_let_before =
	{ syntax_title         = "let before"
	, syntax_patterns      = map exact ["#", "#!"]
	, syntax_code          = ["#  ... = ...", "#! ... = ..."]
	, syntax_description   = join "\n"
		[ "A {{`let`}} expression that can be defined before a guard or function body."
		, "This eases the syntax of sequential actions."
		, ""
		, "Multiple `#` statements can be combined on multiple lines, or separated by `;`."
		]
	, syntax_doc_locations = [CLR "3.5.4"]
	, syntax_examples      =
		[ add_imports ["StdEnv"] $ EX $ join "\n"
		  [ "readchars :: *File -> *([Char], *File)"
		  , "readchars f"
		  , "# (ok,c,f) = freadc f"
		  , "| not ok   = ([], f)"
		  , "# (cs,f)   = readchars f"
		  , "  cs       = [c:cs]"
		  , "= (cs, f)"
		  ]
		]
	}

bs_list_expressions =
	{ syntax_title         = "list expression"
	, syntax_patterns      = map exact ["list", "[]", "[|]", "[:]", ":"] ++ [match "^\\['"]
	, syntax_code          = ["[]", "[...:...]", "[..., ..., ...]", "['...']"]
	, syntax_description   = join "\n"
		[ "A list can be composed of individual elements or a head and a tail. Special syntax is available for creating `[{{Char}}]` lists."
		, ""
		, "See also {{dotdot}} expressions."
		, ""
		, "The colon is not an operator in Clean, because it must always be surrounded by `[` and `]`. It can therefore not be curried, flipped, etc."
		, ""
		, "The `with [!]` annotation makes the compiler use head-strict lists for all unannotated list expressions in a module (see {{`with`}})."
		]
	, syntax_doc_locations = [CLR "4.2.1"]
	, syntax_examples      = map (EXs "macro")
		[ "abc = ['a', 'b', 'c']      // Individual elements"
		, "abc = ['a':['b':['c':[]]]] // Head and tail, ending with the empty list"
		, "abc = ['abc']              // Syntax shorthand for [Char] lists"
		, "abc ['abc':rest] = True    // The shorthand can also be used in patterns"
		] ++ map (add_imports ["_SystemStrictLists"] o EXs "macro")
		[ "abc = [#'abc']             // `#` and `!` may be added to the start/end of the expression to create unboxed and strict lists"
		, "abc = [|'abc']             // An overloaded list expression (can be of any type `l Char | List l Char`)"
		]
	}

bs_macro =
	{ syntax_title         = "macro"
	, syntax_patterns      = map exact [":==", "macro"]
	, syntax_code          = ["... :== ..."]
	, syntax_description   = join "\n"
		[ "A macro is a compile-time rewrite rule. It can be used for constants, inline subtitutions, renaming functions, conditional compilation, etc."
		, ""
		, "Macros can appear in patterns to match on constants."
		]
	, syntax_doc_locations = [CLR "10.3"]
	, syntax_examples      = map (EXs "macro")
		[ "flip f a b :== f b a                    // Useful for currying"
		, "IF_INT_64_OR_32 int64 int32 :== int64   // Conditional compilation"
		, "(o) infixr 9                            // Function composition. Doing this at run-time would be slow\n(o) f g :== \\x -> f (g x)"
		]
	}

bs_module =
	{ syntax_title         = "module heading"
	, syntax_patterns      = map exact ["module", "definition", "implementation", "system", "definition module", "implementation module", "system module"]
	, syntax_code          = ["[definition,implementation,system] module ..."]
	, syntax_description   = "The heading of a Clean file. Definition modules describe what things are exported (dcl files), implementation modules how they are implemented (icl files)."
	, syntax_doc_locations = [CLR "2.2"]
	, syntax_examples      = map EX
		[ "definition module StdList     // Exported definitions of list functions"
		, "implementation module StdList // The implementations of the functions"
		, "module test                   // An implementation module without corresponding dcl"
		, "system module StdInt          // The definitions of a module that contains foreign code (see section 2.6 of the language report)"
		]
	}

bs_newtype =
	{ syntax_title         = "newtype definition"
	, syntax_patterns      = map exact ["=:", "newtype"]
	, syntax_code          = [":: ... =: ... ..."]
	, syntax_description   = join "\n"
		[ "A newtype is a type synonym at run-time but treated as a real type at compile-time."
		, "This allows the creation of separate instances without overhead."
		]
	, syntax_doc_locations = []
	, syntax_examples      =
		[ EX ":: T =: T Int"
		, EX ":: T a =: T a"
		]
	}
bs_newtype_abstract =
	{ syntax_title         = "abstract newtype definition"
	, syntax_patterns      = map exact ["=:", "newtype"]
	, syntax_code          = [":: ... (=: ... ...)"]
	, syntax_description   = join "\n"
		[ "Used in a definition module, this exports a newtype as an abstract data type."
		, "The corresponding newtype needs to be defined in the implementation module without parentheses."
		, "The constructor may not be used outside this module, so to the programmer this is identical to an abstract data type."
		, "However, the compiler needs to have this information when importing the module so it needs to be in the definition module."
		]
	, syntax_doc_locations = []
	, syntax_examples      = [EX ":: Stack a (=: Stack [a])"]
	}

bs_overloaded_maybe_constructors =
	{ syntax_title         = "overloaded maybe constructors"
	, syntax_patterns      = map exact ["?|", "?|None", "?|Just"]
	, syntax_code          = ["?|None", "?|Just"]
	, syntax_description   = "These are overloaded constructors for the different maybe types ({{`?`}}, {{`?^`}}, and {{`?#`}})."
	, syntax_doc_locations = []
	, syntax_examples      = map (add_imports ["_SystemStrictMaybes"] o EX)
		[ "Start :: [?Int] // We have to make the type explicit\nStart = [?|None, ?|Just 37]"
		]
	}

bs_overloaded_type_variable =
	{ syntax_title         = "overloaded type variable"
	, syntax_patterns      = [exact "^", fullMatch "\\w\\^"]
	, syntax_code          = ["... :: ...^"]
	, syntax_description   = "A pattern match on the type of a dynamic depending on the type of the function."
	, syntax_doc_locations = [CLR "8.2.5"]
	, syntax_examples      =
		[ EX $
		  "unpack :: Dynamic -> ?a | TC a\n" +
		  "unpack (x :: a^) = ?Just x // Only values of type a\n" +
		  "unpack _         = ?None"
		]
	}

bs_pattern_named =
	{ syntax_title         = "pattern match"
	, syntax_patterns      = map exact ["=:"]
	, syntax_code          = ["...=:(...)"]
	, syntax_description   = "Give a name to the expression of a pattern to be able to use the whole expression without creating new graphs."
	, syntax_doc_locations = [CLR "3.2"]
	, syntax_examples      = map (add_imports ["_SystemStrictMaybes"] o EX)
		[ "isJustU e=:(?Just _) = (True, e) // On an ADT"
		, ":: Position = {px :: Int, py :: Int}\ngetx p=:{px} = (px, p) // On a record; this has type :: Position -> (Int, Position)"
		]
	}

bs_pattern_predicate =
	{ syntax_title         = "matches pattern expression"
	, syntax_patterns      = map exact ["=:"]
	, syntax_code          = ["...=:(...)"]
	, syntax_description   = join "\n"
		[ "Check whether an expression matches a certain pattern (undocumented)."
		, "The result has type `Bool`."
		, "It is not possible to introduce new identifiers this way."
		, "For instance, one cannot use `if (mbx=:(?Just x)) x 0`."
		, "Also, `=:` can not be used in prefix form because it is not an actual operator but a builtin."
		]
	, syntax_doc_locations = [CLR "3.4.3"]
	, syntax_examples      = [EX "isSingleton l = l =: [_] // Match a value with a pattern"]
	}

bs_qualified_identifier =
	{ syntax_title         = "qualified identifier"
	, syntax_patterns      = [match "^'.+'", exact "qualified"]
	, syntax_code          = ["'...'. ..."]
	, syntax_description   = "The identifiers of {{`qualified`}} imports must be prepended with `'...'.`, where `...` is the name of the qualified import."
	, syntax_doc_locations = [CLR "D.1.1"]
	, syntax_examples      =
		[ add_imports ["StdEnum"] $ EX
		  "import qualified StdList\nStart = 'StdList'.sum [0..10]"
		, add_imports ["StdEnum"] $ requires_itask_compiler $ EX
		  "import qualified StdList as L // requires the iTask compiler\nStart = 'L'.sum [0..10]"
		]
	}

bs_record_disambiguation =
	{ syntax_title         = "record disambiguation"
	, syntax_patterns      = map exact ["|"]
	, syntax_code          = ["{ ... | ... }"]
	, syntax_description   = "Explicitly indicates the type of a record when it cannot be derived from the field names."
	, syntax_doc_locations = [CLR "5.2"]
	, syntax_examples      = map EX
		[ ":: R1 = {x :: Int, y :: Int}\n:: R2 = {x :: Int, y :: Int}\nStart = {R1 | x=37, y=42}"
		]
	}

bs_selection_array =
	{ syntax_title         = "array selection"
	, syntax_patterns      = map fullMatch ["\\.\\[\\]", "\\.\\[.*\\]", "\\.\\[,.*\\]", "\\.\\[.*,.*\\]"]
	, syntax_code          = [".[i]", ".[i,j,...]"]
	, syntax_description   = "Select an element from a (possibly multidimensional) array. The indexes must have the type {{`Int`}}."
	, syntax_doc_locations = [CLR "4.4.1"]
	, syntax_examples      = map (add_imports ["StdEnv"] o EXs "macro")
		[ "five = {!1,2,3,4,5,6,7,8,9,10}.[4]     // Arrays are zero-indexed"
		, "five = {!{!1,2},{3,4,5},{6,7,8}}.[1,2] // This is equivalent to (...).[1].[2]"
		]
	}
bs_selection_array_unique =
	{ syntax_title         = "unique array selection"
	, syntax_patterns      = map fullMatch ["!\\[\\]", "!\\[.*\\]", "!\\[,.*\\]", "!\\[.*,.*\\]"]
	, syntax_code          = ["![i]", "![i,j,...]"]
	, syntax_description   = "Select an element from a (possibly multidimensional, possibly unique) array and return both the element and the array. The indexes must have the type {{`Int`}}."
	, syntax_doc_locations = [CLR "4.4.1"]
	, syntax_examples      = map (add_imports ["StdEnv"] o EXs "macro")
		[ "(five,arr) = {!1,2,3,4,5,6,7,8,9,10}![4]"
		, "(five,arr) = {!{!1,2},{3,4,5},{6,7,8}}![1,2]"
		]
	}
bs_selection_record =
	{ syntax_title         = "record selection"
	, syntax_patterns      = map exact ["."]
	, syntax_code          = ["."]
	, syntax_description   = "Select a field from a (possibly multilevel) record."
	, syntax_doc_locations = [CLR "5.2.1"]
	, syntax_examples      = map
		( add_bootstrap ":: Position = {px :: Int, py :: Int}"
		o add_bootstrap ":: Position3D = {pxy :: Position, pz :: Int}"
		o EXs "macro"
		)
		[ "five = {px=5, py=10}.px"
		, "five = {pxy={px=5, py=10}, pz=2}.pxy.px"
		, "five = {px=5, py=10}.Position.px // If multiple records have a field px, the type name can be used for disambiguation"
		]
	}
bs_selection_record_unique =
	{ syntax_title         = "unique record selection"
	, syntax_patterns      = map exact ["!"]
	, syntax_code          = ["!"]
	, syntax_description   = "Select a field from a (possibly multilevel, possibly unique) record and return both the field data and the record."
	, syntax_doc_locations = [CLR "5.2.1"]
	, syntax_examples      = map
		( add_bootstrap ":: Position = {px :: Int, py :: Int}"
		o add_bootstrap ":: Position3D = {pxy :: Position, pz :: Int}"
		o EXs "macro"
		)
		[ "(five,rec) = {px=5, py=10}!px"
		, "(five,rec) = {pxy={px=5, py=10}, pz=2}!pxy.px // Only the first field should have the exclamation mark"
		, "(five,rec) = {px=5, py=10}!Position.px // If multiple records have a field px, the type name can be used for disambiguation\n" +
		  "                                       // The language report is erroneous here. It is !Position.px, not .Position!px."
		]
	}

bs_strict =
	{ syntax_title         = "strictness annotation"
	, syntax_patterns      = map exact ["strict", "!"]
	, syntax_code          = ["!"]
	, syntax_description   = "Override the lazy evaluation strategy: the argument must be evaluated to head normal form before the function is entered."
	, syntax_doc_locations = [CLR "3.7.5", CLR "10"]
	, syntax_examples      = [EX "acker :: !Int !Int -> Int"]
	}

bs_synonym =
	{ syntax_title         = "synonym type definition"
	, syntax_patterns      = map exact ["synonym", ":=="]
	, syntax_code          = [":: ... :== ..."]
	, syntax_description   = "Defines a new type name for an existing type."
	, syntax_doc_locations = [CLR "5.3"]
	, syntax_examples      = map EX
		[ ":: String :== {#Char}"
		, ":: Operator a :== a a -> a"
		]
	}
bs_synonym_abstract =
	{ syntax_title         = "abstract synonym type definition"
	, syntax_patterns      = map exact ["synonym", ":=="]
	, syntax_code          = [":: ... (:== ...)"]
	, syntax_description   = join "\n"
		[ "Used in a definition module, this defines a new type name for an existing type."
		, "The corresponding synonym needs to be defined in the implementation module without parentheses."
		, "The equivalence may not be used outside this module, so to the programmer this is identical to an abstract data type."
		, "This allows compiler optimisations on abstract types."
		]
	, syntax_doc_locations = [CLR "5.4.1"]
	, syntax_examples      = [EX ":: Stack a (:== [a])"]
	}

bs_type_definition =
	{ syntax_title         = "type definition"
	, syntax_patterns      = map exact ["::", "=", "|"]
	, syntax_code          = [":: ..."]
	, syntax_description   = "Defines a new type. There are too many possibilities to list here; see the documentation."
	, syntax_doc_locations = [CLR "5"]
	, syntax_examples      = map EX
		[ join "\n\t" [":: Day // An algebraic data type","= Mon","| Tue","| Wed","| Thu","| Fri","| Sat","| Sun"]
		, ":: Position = // A record type\n\t{ x :: Int\n\t, y :: Int\n\t}"
		]
	}

bs_type_specification =
	{ syntax_title         = "type specification"
	, syntax_patterns      = map exact ["::"]
	, syntax_code          = ["... :: ..."]
	, syntax_description   = "Specifies the type of a function."
	, syntax_doc_locations = [CLR "3.7"]
	, syntax_examples      = map EX
		[ "map :: (a -> b) [a] -> [b]    // map has arity 2\nmap f []     = []\nmap f [x:xs] = [f x:map f xs]"
		, "map :: (a -> b) -> [a] -> [b] // map has arity 1\nmap f = \\xs -> case xs of\n\t[]    -> []\n\t[x:xs] -> [f x:map f xs]"
		]
	}

bs_unique =
	{ syntax_title         = "uniqueness annotation"
	, syntax_patterns      = map exact [",", "<="] ++ map fullMatch ["\\*", "\\.", "\\w:", "\\[.*<=.*\\]"]
	, syntax_code          =
		[ "*..."
		, ". ..."
		, "...:..., [...<=...], [...<=...], ..."
		]
	, syntax_description   = join "\n"
		[ "Annotates a type with its uniqueness."
		, "A type can either be unique (`*`), not unique (not annotated), possibly unique (`.`) or relatively unique (identifier and `, [...<=...]` after the type context)."
		, "Because uniqueness is binary, there is only one case where `[u<=v]` is not satisfied; when `u` is unique but `v` is not."
		, ""
		, "On function types, *uniqueness propagation* is implicit (see section 9.2 of the language report)."
		, "However, when using unique types in a function or an ADT this has to be made explicit; for instance:"
		, "`T = T (Int, *File)` has to be `T = T *(Int, *File)`."
		, "Functions have to be split up into arity 1 and the subfunctions must be annotated as well; for instance:"
		, "`T = T (Int *Int -> *Int)` has to be `T = T (Int -> *(*Int -> *Int))`."
		]
	, syntax_doc_locations = [CLR "9.1"]
	, syntax_examples      = map (add_imports ["StdEnv"] o EX)
		[ "Start :: *World -> *World   // World is unique"
		, "copyArray :: *(a e) -> *(*a e, *a e) | Array a e // Add parentheses when needed"
		, "f :: .a -> .a               // f works on unique and non-unique values"
		, "f :: v:a u:b -> u:b, [v<=u] // f works when a is less unique than b"
		]
	}

bs_update_array =
	{ syntax_title         = "array update"
	, syntax_patterns      = [exact "&", fullMatch "\\{.*&.*\\[.*].*=.*\\}"]
	, syntax_code          =
		[ "{ a & [i]=x, [j]=y, ... }        // Updates a by setting index i to x, j to y, ..."
		, "# a & [i]=x, [j]=y, ...          // Same as # a = {a & [i]=x, [j]=y, ...}" // See https://clean.cs.ru.nl/Clean_2.3
		, "{ a & [i]=i \\\\ i <- [0,2..9] } // Fancy things can be done if you combine this with a ZF-expression. E.g. give the first 5 even indices the value of their index"
		, "# a & [i]=i \\\\ i <- [0,2..9]   // Also let-before can use generators"
		]
	, syntax_description   = "Updates an array by creating a copy and replacing one or more elements."
	, syntax_doc_locations = [CLR "4.4.1"]
	, syntax_examples      = []
	}
bs_update_record =
	{ syntax_title         = "record update"
	, syntax_patterns      = [exact "&", exact "|", fullMatch "\\{.*&.*=.*\\}"]
	, syntax_code          =
		[ "{ r & f1=x, f2=y, ... } // Updates r by setting f1 to x, f2 to y, ..."
		, "{ MyRecord | r & f1=x, f2=y, ... } // explicitly stating the type"
		, "# r & f1=x, f2=y, ...   // Same as # r = {r & f1=x, f2=y, ...}" // See https://clean.cs.ru.nl/Clean_2.3
		]
	, syntax_description   = "Updates a record by creating a copy and replacing one or more fields."
	, syntax_doc_locations = [CLR "5.2.1"]
	, syntax_examples      = []
	}

bs_where_class =
	{ syntax_title         = "where"
	, syntax_patterns      = map exact ["where"]
	, syntax_code          = ["where"]
	, syntax_description   = "Introduces the members of a {{`class`}} definition."
	, syntax_doc_locations = [CLR "6.1"]
	, syntax_examples      = [EX "class Arith a        // Class definition\nwhere\n\t(+) infixl 6 :: a a -> a\n\t(-) infixl 6 :: a a -> a"]
	}
bs_where_instance =
	{ syntax_title         = "where"
	, syntax_patterns      = map exact ["where"]
	, syntax_code          = ["where"]
	, syntax_description   = "Introduces the implementation of an {{`instance`}}."
	, syntax_doc_locations = [CLR "6.1"]
	, syntax_examples      =
		[ add_imports ["StdEnv"] $ EX $
		  ":: T = C Int\n" +
		  "instance + T\n" +
		  "where\n" +
		  "\t(+) (C x) (C y) = C (x+y)"
		]
	}
bs_where_local =
	{ syntax_title         = "where"
	, syntax_patterns      = map exact ["where"]
	, syntax_code          = ["where"]
	, syntax_description   = "Introduces local definitions. For guard-local definitions, see {{`with`}}."
	, syntax_doc_locations = [CLR "3.5.2"]
	, syntax_examples      =
		[ add_imports ["StdEnv"] $
		  EXs "macro" $
		  "primes = sieve [2..] // Local definitions\n" +
		  "where\n" +
		  "\tsieve [pr:r] = [pr:sieve (filter (\\n -> n rem pr <> 0) r)]"
		]
	}

bs_with =
	{ syntax_title         = "with"
	, syntax_patterns      = map exact ["with"]
	, syntax_code          = ["with"]
	, syntax_description   = "Introduces guard-local definitions. For function-local definitions, see {{`where`}}."
	, syntax_doc_locations = [CLR "3.5.3"]
	, syntax_examples      =
		[ add_imports ["StdEnv"] $
		  EXs "macro" $
		  "f g x y\n" +
		  "| x < 0 = g local\n" +
		  "\twith local = x + y\n" +
		  "| x > 0 = g local\n" +
		  "\twith local = x - y\n" +
		  "| otherwise = g 0"
		]
	}

bs_with_compiler_settings =
	{ syntax_title         = "with"
	, syntax_patterns      = map exact ["with", "[]", "[!]"]
	, syntax_code          = ["module ... with ..."]
	, syntax_description   = join "\n"
		[ "Sets compiler switches for the current module."
		, ""
		, "Currently, there is one switch: `with [!]` makes lists head-strict, and `with []` makes them lazy (the default)."
		, "This means that lists like `[x,y,z]`, list comprehensions like `[x \\\\ ...]`, and the `<-` generator in list comprehensions are head-strict instead of lazy."
		, "The type of lists can still be fixed separate from the module-level switch by indicating it in individual expressions (e.g. `[!x,y,z]`, `[^x \\\\ ...]`, or `<!-`)."
		]
	, syntax_doc_locations = []
	, syntax_examples      = map EX
		[ "module test with [!] // makes lists head-strict"
		]
	}

bs_zf =
	{ syntax_title         = "list comprehension"
	, syntax_patterns      = map exact ["<-", "<!-", "<^-", "<|-", "<-:", "\\\\", ",", "&", "|", "let"]
	, syntax_code          =
		[ "[... \\\\ ... <- ...]"
		, "{... \\\\ ... <- ...}"
		]
	, syntax_description   = "Constructs a list or array composed of elements drawn from other lists or arrays. It is possible to use local definitions as well (see {{`let`}})."
	, syntax_doc_locations = [CLR "4.2.1", CLR "4.4.1"]
	, syntax_examples      = map (add_imports ["StdEnv", "_SystemStrictMaybes"] o EXs "macro")
		[ "cartesian      = [(x,y) \\\\ x <- [1,2,3], y <- [10,20]] // Cartesian product: (1,10), (1,20), (2,10), (2,20), (3,10), (3,20)"
		, "zip xs ys      = [(x,y) \\\\ x <- xs & y <- ys] // Pairwise zip through the lists"
		, "filter f xs    = [x \\\\ x <- xs | f x]         // Guard to add conditions"
		, "catMaybes ms   = [x \\\\ ?Just x <- ms]         // Pattern matching in the selector"
		, "triangle       = [(x,y) \\\\ x <- [1,2,3], y <- [1..x]]  // Reusing x in the next generator: (1,1), (2,1), (2,2), (3,1), (3,2), (3,3)"
		, "arrToList a    = [x \\\\ x <-: a]               // <-: to draw elements from an array"
		, "castList xs    = [x \\\\ x <!- xs]              // <!- to draw elements from a head-strict list; <^- for a lazy list; <|- for an overloaded list"
		, "castList xs    = [|x \\\\ x <|- xs]             // The two pipe characters make both xs and the result overloaded lists"
		, "listToArr xs   = {x \\\\ x <- xs}               // {..} to create an array"
		, "toLazyArray xs = {^x \\\\ x <- xs}              // {^..} to create a lazy array (similarly for other array and list variants)"
		] ++
		[ add_imports ["StdEnv"] $ EXs "rhs" $
		  "[ sx \\\\ x <- [0..]\n" +
		  "\t, let sx = toString x                     // The let must end with a newline\n" +
		  "\t]"
		]
	}
