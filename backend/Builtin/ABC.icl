implementation module Builtin.ABC

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import _SystemArray
import StdList
import StdMisc

import Data.Either
import Text

import Cloogle.API
import Cloogle.DB

builtin_abc_instructions :: [Either String ABCInstructionEntry]
builtin_abc_instructions =
	withTitle "Arithmetic" arith_instructions ++
	withTitle "Stack manipulation" stack_operations ++
	withTitle "Branching" branches ++
	withTitle "Miscellaneous" miscellaneous ++
	withTitle "Directives" directives ++
	withTitle "Undocumented instructions" [{zero & aie_instruction=i} \\ i <- other_instructions]
where
	withTitle title instrs = [Left title:map Right instrs]

instance zero ABCInstructionEntry
where
	zero =
		{ aie_instruction = ""
		, aie_arguments   = []
		, aie_description = "There is no documentation for this ABC instruction yet."
		}

LABEL    :== ABCArgument ABCTypeLabel        False
LABEL_   :== ABCArgument ABCTypeLabel        True
A_OFFSET :== ABCArgument ABCTypeAStackOffset False
B_OFFSET :== ABCArgument ABCTypeBStackOffset False
A_SIZE   :== ABCArgument ABCTypeAStackSize   False
B_SIZE   :== ABCArgument ABCTypeBStackSize   False
STRING   :== ABCArgument ABCTypeString       False
STRING_  :== ABCArgument ABCTypeString       True
BOOL     :== ABCArgument ABCTypeBool         False
BOOL_    :== ABCArgument ABCTypeBool         True
CHAR     :== ABCArgument ABCTypeChar         False
CHAR_    :== ABCArgument ABCTypeChar         True
INT      :== ABCArgument ABCTypeInt          False
INT_     :== ABCArgument ABCTypeInt          True
REAL     :== ABCArgument ABCTypeReal         False
REAL_    :== ABCArgument ABCTypeReal         True

arith_instructions :: [ABCInstructionEntry]
arith_instructions =
	[ arith1 "absR"    "Real" "absolute value"
	, arith1 "acosR"   "Real" "arccosine"
	, arith1 "asinR"   "Real" "arcsine"
	, arith1 "atanR"   "Real" "arctangent"
	, arith1 "cosR"    "Real" "cosine"
	, arith1 "entierR" "Real" "(Int) entier (floor)"
	, arith1 "expR"    "Real" "exponential function (e^r)"
	, arith1 "lnR"     "Real" "natural logarithm"
	, arith1 "log10R"  "Real" "base-10 logarithm"
	, arith1 "notB"    "Bool" "logical negation"
	, arith1 "sinR"    "Real" "sine"
	, arith1 "sqrtR"   "Real" "square root"
	, arith1 "tanR"    "Real" "tangent"

	, op1 "clzb" "Int"  "Counts the leading zero bits of"
	, op1 "decI" "Int"  "Decrements"
	, op1 "incI" "Int"  "Increments"
	, op1 "negI" "Int"  "Negates"
	, op1 "negR" "Real" "Negates"
	, op1 "not%" "Int"  "Bitwise negates"

	, op2 "addI"    "Int"  "Sums"
	, op2 "addR"    "Real" "Sums"
	, op2 "andB"    "Bool" "Logically conjuncts"
	, op2 "and%"    "Int"  "Bitwise conjuncts"
	, op2 "divI"    "Int"  "Divides"
	, op2 "divR"    "Real" "Divides"
	, op2 "eqB"     "Bool" "Checks equality on"
	, op2 "eqC"     "Char" "Checks equality on"
	, op2 "eqI"     "Int"  "Checks equality on"
	, op2 "eqR"     "Real" "Checks equality on"
	, i_eqAC_a
	, op2 "gtC"     "Char" "Checks greater-than on"
	, op2 "gtI"     "Int"  "Checks greater-than on"
	, op2 "gtR"     "Real" "Checks greater-than on"
	, op2 "ltC"     "Char" "Checks less-than on"
	, op2 "ltI"     "Int"  "Checks less-than on"
	, op2 "ltR"     "Real" "Checks less-than on"
	, op2 "mulI"    "Int"  "Multiplies"
	, op2 "mulR"    "Real" "Multiplies"
	, op2 "orB"     "Bool" "Logically disjuncts"
	, op2 "or%"     "Int"  "Bitwise disjuncts"
	, op2 "powR"    "Real" "Raises to the power on"
	, op2 "remI"    "Int"  "Computes the remainder after division of"
	, op2 "rotl%"   "Int"  "Bitwise left-rotate on"
	, op2 "rotr%"   "Int"  "Bitwise right-rotate on"
	, op2 "shiftl%" "Int"  "Bitwise left-shift on"
	, op2 "shiftr%" "Int"  "Bitwise right-shift on"
	, op2 "subI"    "Int"  "Subtracts"
	, op2 "subR"    "Real" "Subtracts"
	, op2 "xor%"    "Int"  "Bitwise XOR on"

	, eq_arg "Bool" BOOL 'A'
	, eq_arg "Bool" BOOL 'B'
	, eq_arg "Char" CHAR 'A'
	, eq_arg "Char" CHAR 'B'
	, eq_arg "Int"  INT  'A'
	, eq_arg "Int"  INT  'B'
	, eq_arg "Real" REAL 'A'
	, eq_arg "Real" REAL 'B'

	, convert "CtoI"  "Char" "Int"
	, convert "CtoAC" "Char" "String"
	, convert "ItoC"  "Int"  "Char"
	, convert "ItoR"  "Int"  "Real"
	, convert "RtoI"  "Real" "Int"
	]
where
	arith1 :: !String !String !String -> ABCInstructionEntry
	arith1 instr type description =
		{ zero
		& aie_instruction = instr
		, aie_description = "Computes the " + description + " of the " + type + " on top of the B-stack."
		}

	op1 :: !String !String !String -> ABCInstructionEntry
	op1 instr type description =
		{ zero
		& aie_instruction = instr
		, aie_description = description + " the " + type + " on top of the B-stack."
		}

	op2 :: !String !String !String -> ABCInstructionEntry
	op2 instr type description =
		{ zero
		& aie_instruction = instr
		, aie_description = description + " the two " + type + "s on top of the B-stack."
		}

	eq_arg :: !String !ABCArgument !Char -> ABCInstructionEntry
	eq_arg type arg stack =
		{ zero
		& aie_instruction = {'e','q',type.[0],'_',toLower stack}
		, aie_arguments   = [arg, if (stack == 'A') A_OFFSET B_OFFSET]
		, aie_description = "Checks equality between the first argument and the " + {stack} + "-stack element."
		}

	convert :: !String !String !String -> ABCInstructionEntry
	convert instr fr to =
		{ zero
		& aie_instruction = instr
		, aie_description = "Converts the " + fr + " on top of the B-stack to " + to + "."
		}

	i_eqAC_a =
		{ zero
		& aie_instruction = "eqAC_a"
		, aie_arguments   = [STRING]
		, aie_description = "Checks that the string on top of the A-stack equals the argument string."
		}

stack_operations :: [ABCInstructionEntry]
stack_operations =
	[ push    "Bool" BOOL
	, push    "Char" CHAR
	, push    "Int"  INT
	, push    "Real" REAL
	, push_a  "Bool"
	, push_a  "Char"
	, push_a  "Int"
	, push_a  "Real"
	, i_pop_a
	, i_pop_b
	, i_push_a
	, i_push_b
	, i_push_a_b
	, i_push_node
	, i_push_node_u
	, i_push_args
	, i_repl_args
	, build   "Bool" BOOL
	, build   "Char" CHAR
	, build   "Int"  INT
	, build   "Real" REAL
	, build_b "Bool"
	, build_b "Char"
	, build_b "Int"
	, build_b "Real"
	, i_build
	, i_buildh
	, i_build_r
	, i_build_u
	, i_buildAC
	, i_create
	, i_create_array
	, i_create_array_
	, i_select
	, i_update
	, i_replace
	, fill    "Bool" BOOL
	, fill    "Char" CHAR
	, fill    "Int"  INT
	, fill    "Real" REAL
	, fill_b  "Bool"
	, fill_b  "Char"
	, fill_b  "Int"
	, fill_b  "Real"
	, i_fill
	, i_fill_r
	, i_fill_a
	, i_fillh
	, i_fill_u
	, i_fill2
	, i_fill2_r
	, i_fill3
	, i_fill3_r
	, i_eq_desc
	, i_eq_desc_b
	, i_eq_nulldesc
	, i_repl_r_a_args_n_a
	, i_update_a
	, i_update_b
	, i_updatepop_a
	, i_updatepop_b
	]
where
	push :: !String !ABCArgument -> ABCInstructionEntry
	push type arg =
		{ zero
		& aie_instruction = "push" + {type.[0]}
		, aie_arguments   = [arg]
		, aie_description = "Pushes the " + type + " argument to the B-stack."
		}

	push_a :: !String -> ABCInstructionEntry
	push_a type =
		{ zero
		& aie_instruction = "push" + {type.[0]} + "_a"
		, aie_arguments   = [INT]
		, aie_description = "Pushes the " + type + " from the nth position on the A-stack to the B-stack."
		}

	build :: !String !ABCArgument -> ABCInstructionEntry
	build type arg =
		{ zero
		& aie_instruction = "build" + {type.[0]}
		, aie_arguments   = [arg]
		, aie_description = "Builds a " + type + "-node with the argument as value on the A-stack."
		}

	build_b :: !String -> ABCInstructionEntry
	build_b type =
		{ zero
		& aie_instruction = "build" + {type.[0]} + "_b"
		, aie_arguments   = [INT]
		, aie_description = "Builds a " + type + "-node with the value on the nth position of the B-stack on the A-stack."
		}

	fill :: !String !ABCArgument -> ABCInstructionEntry
	fill type arg =
		{ zero
		& aie_instruction = "fill" + {type.[0]}
		, aie_arguments   = [arg, A_OFFSET]
		, aie_description = "Fills the referenced A-stack node with a boxed " + type + " argument."
		}

	fill_b :: !String -> ABCInstructionEntry
	fill_b type =
		{ zero
		& aie_instruction = "fill" + {type.[0]} + "_b"
		, aie_arguments   = [B_OFFSET, A_OFFSET]
		, aie_description = "Fills the referenced A-stack node with a boxed " + type + " argument from the referenced B-stack element."
		}

	i_build =
		{ zero
		& aie_instruction = "build"
		, aie_arguments   = [LABEL, A_SIZE, LABEL]
		, aie_description = join " "
			[ "Builds a thunk with the specified number of arguments starting from the top of the A stack."
			, "The arguments are popped from the A-stack."
			, "The first label is for the descriptor and is typically ignored; the second label is the code address."
			, "For nodes with unboxed arguments {{`build_u`}} is used."
			]
		}

	i_buildh =
		{ zero
		& aie_instruction = "buildh"
		, aie_arguments   = [LABEL, A_SIZE]
		, aie_description = join " "
			[ "Builds a head normal form with the specified number of arguments starting from the top of the A stack."
			, "The arguments are popped from the A-stack."
			, "The first label is for the descriptor."
			]
		}

	i_build_r =
		{ zero
		& aie_instruction = "build_r"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, A_OFFSET, B_OFFSET]
		, aie_description = "Builds a record with the specified number of arguments starting from the referenced offsets."
		}

	i_build_u =
		{ zero
		& aie_instruction = "build_u"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, LABEL]
		, aie_description = join " "
			[ "Builds a thunk with unboxed arguments with the specified number of arguments starting from the tops of the A and B stacks."
			, "The arguments are popped from the B and A stacks."
			, "The first label is for the descriptor and is typically ignored; the second label is the code address."
			]
		}

	i_buildAC =
		{ zero
		& aie_instruction = "buildAC"
		, aie_arguments   = [STRING]
		, aie_description = "Pushes the argument string to the A-stack."
		}

	i_create =
		{ zero
		& aie_instruction = "create"
		, aie_description = "Creates a new empty node and pushes its address to the A-stack."
		}

	i_create_array =
		{ zero
		& aie_instruction = "create_array"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Creates an array on the A-stack."
			, "The elements have type `label` (which can be `_` for boxed arrays)."
			, "The last two arguments indicate the stack sizes of the elements."
			, "The size and initial value are popped from the B and A stacks."
			]
		}
	i_create_array_ =
		{ zero
		& aie_instruction = "create_array_"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Creates an array on the A-stack."
			, "The elements have type `label` (which can be `_` for boxed arrays)."
			, "The last two arguments indicate the stack sizes of the elements."
			, "The size is popped from the B-stack; the elements are initialised as `_Nil` regardless of the type."
			]
		}

	i_select =
		{ zero
		& aie_instruction = "select"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Selects an element from an array and pushes it on the stack it should be stored on."
			, "The elements have type `label` (which can be `_` for boxed arrays)."
			, "The last two arguments indicate the stack sizes of the elements."
			, "The index to select from and the array are popped from the B and A stacks, respectively."
			]
		}

	i_update =
		{ zero
		& aie_instruction = "update"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Updates an element in an array and pushes the updated array on top of the A-stack."
			, "The elements have type `label` (which can be `_` for boxed arrays)."
			, "The last two arguments indicate the stack sizes of the elements."
			, "First, the array to update is popped from the A-stack. The index to update is popped from the B-stack."
			, "Afterwards, the value to update with is popped from the stack(s) it is stored on."
			]
		}

	i_replace =
		{ zero
		& aie_instruction = "replace"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Updates an element in an array and pushes the array as well as the previous value on the stacks."
			, "The elements have type `label` (which can be `_` for boxed arrays)."
			, "The last two arguments indicate the stack sizes of the elements."
			, "First, the array to update is popped from the A-stack. The index to update is popped from the B-stack."
			, "Afterwards, the value to update with is popped from the stack(s) it is stored on."
			, "Finally, the replaced element is pushed on the stack(s), followed by the updated array."
			]
		}

	i_eq_desc =
		{ zero
		& aie_instruction = "eq_desc"
		, aie_arguments   = [LABEL, INT, A_OFFSET]
		, aie_description = join " "
			[ "Checks that the indicated node on the A-stack matches the descriptor given by the label."
			, "The `int` argument is the arity of the descriptor."
			]
		}
	i_eq_desc_b =
		{ zero
		& aie_instruction = "eq_desc_b"
		, aie_arguments   = [LABEL, A_OFFSET]
		, aie_description = join " "
			[ "The indicated node on the A-stack is assumed to be an array."
			, "The instruction checks that the array is of the type indicated by `label`."
			]
		}
	i_eq_nulldesc =
		{ zero
		& aie_instruction = "eq_nulldesc"
		, aie_arguments   = [LABEL, A_OFFSET]
		, aie_description = "Checks that the indicated node on the A-stack matches the descriptor given by the label, ignoring arity."
		}

	i_pop_a =
		{ zero
		& aie_instruction = "pop_a"
		, aie_arguments   = [A_OFFSET]
		, aie_description = "Pops elements off the A-stack until the referenced element."
		}
	i_pop_b =
		{ zero
		& aie_instruction = "pop_b"
		, aie_arguments   = [B_OFFSET]
		, aie_description = "Pops elements off the B-stack until the referenced element."
		}
	i_push_a =
		{ zero
		& aie_instruction = "push_a"
		, aie_arguments   = [A_OFFSET]
		, aie_description = "Pushes the referenced A-stack element on the A-stack."
		}
	i_push_b =
		{ zero
		& aie_instruction = "push_b"
		, aie_arguments   = [B_OFFSET]
		, aie_description = "Pushes the referenced B-stack element on the B-stack."
		}
	i_push_a_b =
		{ zero
		& aie_instruction = "push_a_b"
		, aie_arguments   = [A_OFFSET]
		, aie_description = "Pushes the A-stack element as an integer (i.e., a pointer to the heap) on the B-stack."
		}
	i_push_node =
		{ zero
		& aie_instruction = "push_node"
		, aie_arguments   = [LABEL, A_SIZE]
		, aie_description = join " "
			[ "Pushes all arguments of the node referenced by the top of the A-stack on the A-stack."
			, "This assumes that the node has only A-stack arguments and is of the provided arity."
			, "The descriptor of the node is replaced by the provided label."
			, "For nodes with unboxed arguments {{`push_node_u`}} is used."
			]
		}
	i_push_node_u =
		{ zero
		& aie_instruction = "push_node_u"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE]
		, aie_description = join " "
			[ "Pushes all arguments of the node referenced by the top of the A-stack on the A and B stacks."
			, "This assumes that the node is of the provided arity."
			, "The descriptor of the node is replaced by the provided label."
			]
		}
	i_push_args =
		{ zero
		& aie_instruction = "push_args"
		, aie_arguments   = [A_OFFSET, A_SIZE, A_SIZE]
		, aie_description = join " "
			[ "Pushes all arguments of the referenced A-stack node on the A-stack."
			, "The two A-size arguments are equal and indicate the node's arity."
			]
		}
	i_repl_args =
		{ zero
		& aie_instruction = "repl_args"
		, aie_arguments   = [A_SIZE, A_SIZE]
		, aie_description = join " "
			[ "Replaces the node on top of the A-stack by (all) its arguments."
			, "The two A-size arguments are equal and indicate the node's arity."
			]
		}

	i_fill =
		{ zero
		& aie_instruction = "fill"
		, aie_arguments   = [LABEL, A_SIZE, LABEL, A_OFFSET]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a thunk with the specified code address and a number of elements from the top of the A-stack."
			, "The arguments are popped from the A-stack."
			, "The first label is the descriptor, which is typically ignored; the second is the code address."
			]
		}

	i_fill_r =
		{ zero
		& aie_instruction = "fill_r"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, A_OFFSET, A_OFFSET, B_OFFSET]
		, aie_description = join " "
			[ "Fills the referenced A-stack node (first A-offset) as a record with the given label as descriptor."
			, "The A-size and B-size specify the number of arguments from the A and the B-stack."
			, "The second A-offset and B-offset specify the first argument from each stack, respectively."
			]
		}

	i_fill_a =
		{ zero
		& aie_instruction = "fill_a"
		, aie_arguments   = [A_OFFSET, A_OFFSET]
		, aie_description = join " "
			[ "Fills the referenced A-stack node (the second argument, a thunk) as a head normal form"
			, "with the descriptor and elements from the first argument (a HNF)."
			]
		}

	i_fillh =
		{ zero
		& aie_instruction = "fillh"
		, aie_arguments   = [LABEL, A_SIZE, A_OFFSET]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a head normal form with a number of elements from the top of the A-stack."
			, "The arguments are popped from the A-stack. The label is the descriptor."
			]
		}

	i_fill_u =
		{ zero
		& aie_instruction = "fill_u"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, LABEL, A_OFFSET]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a thunk with unboxed arguments"
			, "with the specified code address and a number of elements from the tops of the A and B stacks."
			, "The arguments are popped from the B and A stacks."
			, "The first label is the descriptor, which is typically ignored; the second is the code address."
			]
		}
	i_fill2 =
		{ zero
		& aie_instruction = "fill2"
		, aie_arguments   = [LABEL, A_SIZE, A_OFFSET, STRING]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a HNF with arity > 2 and without B arguments."
			, "Whether the descriptor is updated is indicated by the first element of the bit vector."
			, "The remaining elements indicate which arguments are updated with arguments popped from the A-stack."
			, "The instruction assumes that the argument block already exists."
			]
		}
	i_fill2_r =
		{ zero
		& aie_instruction = "fill2_r"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, A_OFFSET, STRING]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a record with arity > 2."
			, "Whether the descriptor is updated is indicated by the first element of the bit vector."
			, "The remaining elements indicate which arguments are updated with arguments popped from the A and B-stack."
			, "The instruction assumes that the argument block already exists."
			]
		}
	i_fill3 =
		{ zero
		& aie_instruction = "fill3"
		, aie_arguments   = [LABEL, A_SIZE, A_OFFSET, STRING]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a HNF with arity > 2 and without B arguments,"
			, "with the given descriptor and the arguments of a HNF node popped from the A-stack."
			, "The popped node is reserved and the second block is reused."
			, "The arguments indicated by the bit vector are updated with arguments popped from the A-stack."
			]
		}

	i_fill3_r =
		{ zero
		& aie_instruction = "fill3_r"
		, aie_arguments   = [LABEL, A_SIZE, B_SIZE, A_OFFSET, STRING]
		, aie_description = join " "
			[ "Fills the referenced A-stack node as a record with arity > 2 with the given descriptor and the arguments of a record node popped from the A-stack."
			, "The popped node is reserved and the second block is reused."
			, "The arguments indicated by the bit vector are updated with arguments popped from the A and B-stack."
			]
		}
	i_repl_r_a_args_n_a =
		{ zero
		& aie_instruction = "repl_r_a_args_n_a"
		, aie_description = join " "
			[ "Replaces a record at the top of the A-stack by its pointer arguments."
			, "Additionally pushes the number of these pointers on the B-stack."
			]
		}

	i_update_a =
		{ zero
		& aie_instruction = "update_a"
		, aie_arguments   = [A_OFFSET, A_OFFSET]
		, aie_description = join " "
			[ "Replaces the referenced A-stack element (second argument)"
			, "with the value from the nth position on the A-stack (first argument)."
			]
		}

	i_update_b =
		{ zero
		& aie_instruction = "update_b"
		, aie_arguments   = [B_OFFSET, B_OFFSET]
		, aie_description = join " "
			[ "Replaces the referenced B-stack element (second argument)"
			, "with the value from the nth position on the B-stack (first argument)."
			]
		}

	i_updatepop_a =
		{ zero
		& aie_instruction = "updatepop_a"
		, aie_arguments   = [A_OFFSET, A_OFFSET]
		, aie_description = join " "
			[ "Replaces the referenced A-stack element (second argument)"
			, "with the value from the nth position on the A-stack (first argument)."
			, "Afterwards, pops elements off the A-stack until the referenced element."
			, "This is the same as the sequence `update_a i j`, `pop_a j`."
			]
		}

	i_updatepop_b =
		{ zero
		& aie_instruction = "updatepop_b"
		, aie_arguments   = [B_OFFSET, B_OFFSET]
		, aie_description = join " "
			[ "Replaces the referenced B-stack element (second argument)"
			, "with the value from the nth position on the B-stack (first argument)."
			, "Afterwards, pops elements off the B-stack until the referenced element."
			, "This is the same as the sequence `update_b i j`, `pop_b j`."
			]
		}

branches :: [ABCInstructionEntry]
branches =
	[ i_jmp
	, i_jmp_false
	, i_jmp_true
	, i_jmpD
	, i_jmp_eval
	, i_jmp_eval_upd
	, i_jsr
	, i_jsr_eval
	, i_rtn
	]
where
	i_jmp =
		{ zero
		& aie_instruction = "jmp"
		, aie_arguments   = [LABEL]
		, aie_description = "Unconditional jump to a label."
		}
	i_jmp_false =
		{ zero
		& aie_instruction = "jmp_false"
		, aie_arguments   = [LABEL]
		, aie_description = "Jump to a label if the Bool on top of the B-stack is false."
		}
	i_jmp_true =
		{ zero
		& aie_instruction = "jmp_true"
		, aie_arguments   = [LABEL]
		, aie_description = "Jump to a label if the Bool on top of the B-stack is true."
		}
	i_jmpD =
		{ zero
		& aie_instruction = "jmpD"
		, aie_arguments   = [CHAR, CHAR, LABEL, A_SIZE, LABEL, LABEL]
		, aie_description = join " "
			[ "Compare the descriptor on top of the B-stack to the first label according to the characters:"
			, "`a` stands for above, `b` for below, and `e` for equal."
			, "The order of the descriptors follows the order in which the descriptors are defined in the ABC file."
			, "The ordering of descriptors from different modules is undefined."
			, "If the first comparison is true, jump to the second label argument."
			, "If not, but the second comparison is true, jump to the third label argument."
			, "The A-size gives the arity of the descriptor."
			, "The characters are given without quotes."
			]
		}
	i_jmp_eval =
		{ zero
		& aie_instruction = "jmp_eval"
		, aie_arguments   = []
		, aie_description = join " "
			[ "If the node on top of the A-stack is a thunk, jump to evaluate it."
			, "Otherwise, return to the calling subroutine."
			, "This is the tail call equivalent of {{`jsr_eval`}}."
			]
		}
	i_jmp_eval_upd =
		{ zero
		& aie_instruction = "jmp_eval_upd"
		, aie_arguments   = []
		, aie_description = join " "
			[ "Pop the node on top of the A-stack from the stack."
			, "If it is a head normal form, fill the new top of the stack with it and return."
			, "If it is a thunk, jump to the code generated by the associated {{`.n`}} annotation,"
			, "which evaluates the thunk and then also fills the new top of the stack with it and returns."
			]
		}
	i_jsr =
		{ zero
		& aie_instruction = "jsr"
		, aie_arguments   = [LABEL]
		, aie_description = "Subroutine jump to a label. {{`rtn`}} returns to the instruction after this `jsr`."
		}
	i_jsr_eval =
		{ zero
		& aie_instruction = "jsr_eval"
		, aie_arguments   = [A_OFFSET, LABEL]
		, aie_description = "Subroutine jump to evaluate the indicated A-stack element. {{`rtn`}} returns to the instruction after this `jsr_eval`."
		}
	i_rtn =
		{ zero
		& aie_instruction = "rtn"
		, aie_description = "Returns from a subroutine call (e.g. {{`jsr`}})."
		}

miscellaneous :: [ABCInstructionEntry]
miscellaneous =
	[ i_ccall
	, i_centry
	, i_get_node_arity
	, i_halt
	, i_instruction
	, i_load_i
	, i_load_si16
	, i_load_si32
	, i_load_ui8
	, i_no_op
	, i_push_r_arg_t
	, i_push_t_r_a
	, i_push_t_r_args
	]
where
	i_ccall =
		{ zero
		& aie_instruction = "ccall"
		, aie_arguments   = [LABEL, STRING]
		, aie_description = join "\n"
			[ "Calls a C function."
			, "Some of this is documented in https://svn.cs.ru.nl/repos/clean-tools/trunk/htoclean/CallingCFromClean.html."
			, "The first argument is the name of the function, the second is the signature."
			, "\n"
			, "The signature has to be of the form `flags?input?sep output(sep state)?`, where"
			, "  - `sep`    can be either `-` or `:`."
			, "  - `flags`  can be `G` and/or `P`. `G` saves the state in global variables and is needed when the called C function will call Clean functions. `P` lets the callee pop arguments (necessary on 32-bit Windows)."
			, "  - `input`  is a number of input argument types (allowed: `IpRrSsAOF`; see below)."
			, "  - `output` is a number of output argument types (allowed: `VIpRrSsAOF`; see below)."
			, "  - `state`  is a carried state that is not passed to the C function, for example used to thread {{`World`}} in ccalls (allowed: `IpRSA`)."
			, "\n"
			, "Input, output and state argument types can be:"
			, "  - `I`    for 32-bit integers"
			, "  - `p`    for pointers (on 32-bit systems this is identical to `I`)"
			, "  - [`Rr`] for reals (`R` for `double`s and `r` for `float`s)"
			, "  - `S`    for Clean Strings (`{#Char}`). If used as input type this passes a pointer to the string's length (number of characters). The actual array is at offset 4/8 (32/64 bit). If this is used as output type, the C function has to return a pointer to the string's length, followed by the actual string. A copy of the string will be created in the Clean heap, and this copy will be used by Clean. If the string was allocated in C, for example using malloc, it should be deallocated in C when it is no longer used."
			, "  - `s`    for the characters of a Clean String (handy to use in conjuction with {{`packString`}}, as Clean strings are not null-terminated). The length (number of characters) is at offset -4/-8. (32/64 bit)."
			, "  - `A`    for A-stack elements. A pointer to the third block of the node is passed. For arrays, this is a pointer to the elements. One word higher is the element type. The size of the array is two words higher."
			, "  - [`OF`] for function pointers"
			, "  - `V`    for `void`, packs the following argument types in a tuple (e.g. `VIR` means `(Int, Real)`)"
			]
		}
	i_centry =
		{ zero
		& aie_instruction = "centry"
		, aie_arguments   = [LABEL, LABEL, STRING]
		, aie_description = join "\n"
			[ "Adds code to call a Clean function from C."
			, "Usually it is not needed to write this instruction yourself."
			, "It is generated with the `foreign export` construct.\n"
			, "The first label is the name of the C function to generate."
			, "The second label is the Clean function to link it to.\n"
			, "The string argument indicates the type."
			, "For more information, see {{`ccall`}}."
			]
		}

	i_get_node_arity =
		{ zero
		& aie_instruction = "get_node_arity"
		, aie_arguments   = [A_OFFSET]
		, aie_description = "Pushes the arity of the descriptor of the referenced A-stack element to the B-stack."
		}

	i_halt =
		{ zero
		& aie_instruction = "halt"
		, aie_description = "Terminates the program immediately."
		}

	i_instruction =
		{ zero
		& aie_instruction = "instruction"
		, aie_arguments   = [INT]
		, aie_description = "Adds the raw argument as a word in the generated object file."
		}

	i_load_i =
		{ zero
		& aie_instruction = "load_i"
		, aie_arguments   = [INT]
		, aie_description = join "\n\n"
			[ "Take the top of the B-stack as a pointer and read an integer from that pointer with the argument as offset."
			, "See also {{`load_si16`}}, {{`load_si32`}}, {{`load_ui8`}}."
			]
		}
	i_load_si16 =
		{ zero
		& aie_instruction = "load_si16"
		, aie_arguments   = [INT]
		, aie_description = join "\n\n"
			[ "Take the top of the B-stack as a pointer and read a 16-bit signed integer from that pointer with the argument as offset."
			, "See also {{`load_i`}}, {{`load_si32`}}, {{`load_ui8`}}."
			]
		}
	i_load_si32 =
		{ zero
		& aie_instruction = "load_si32"
		, aie_arguments   = [INT]
		, aie_description = join "\n\n"
			[ "Take the top of the B-stack as a pointer and read a 32-bit signed integer from that pointer with the argument as offset."
			, "This instruction is only available on 64-bit systems. On 32-bit systems, {{`load_i`}} has the same effect."
			, "See also {{`load_i`}}, {{`load_si16`}}, {{`load_ui8`}}."
			]
		}
	i_load_ui8 =
		{ zero
		& aie_instruction = "load_ui8"
		, aie_arguments   = [INT]
		, aie_description = join "\n\n"
			[ "Take the top of the B-stack as a pointer and read a 8-bit unsigned integer from that pointer with the argument as offset."
			, "See also {{`load_i`}}, {{`load_si16`}}, {{`load_si32`}}."
			]
		}

	i_no_op =
		{ zero
		& aie_instruction = "no_op"
		, aie_description = join "\n"
			[ "Do nothing. This is for example useful in the `cast` function:\n"
			, "```clean"
			, "cast :: .a -> .b"
			, "cast _ = code {"
			, "\tno_op"
			, "}"
			, "```"
			]
		}

	i_push_r_arg_t =
		{ zero
		& aie_instruction = "push_r_arg_t"
		, aie_description = join " "
			[ "Gets the *n*th element from the type string of a record."
			, "The type string is on top of the B-stack; *n* below that."
			]
		}
	i_push_t_r_a =
		{ zero
		& aie_instruction = "push_t_r_a"
		, aie_arguments   = [A_OFFSET]
		, aie_description = "Push the address of the type string of the referenced record to the B-stack."
		}
	i_push_t_r_args =
		{ zero
		& aie_instruction = "push_t_r_args"
		, aie_description = "Pops a record from the A-stack, pushes its members in reversed order to both of the stacks, then pushes the address of the type string to the B-stack."
		}

directives :: [ABCInstructionEntry]
directives =
	[ d_d
	, d_n
	, d_nu
	, d_o
	, d_r
	, d_export
	, d_inline
	, d_module
	, d_depend
	, d_end
	, d_endinfo
	, d_start
	]
where
	d_d =
		{ zero
		& aie_instruction = ".d"
		, aie_arguments   = [A_OFFSET, B_OFFSET, STRING_]
		, aie_description = join " "
			[ "Indicates how many stack elements are on the stack when a jump or return follows."
			, "The first integer is the number of elements on the A-stack; the second that of B-stack elements."
			, "The optional third argument indicates the type of the B-stack elements, e.g. `bbi` for two booleans and an integer."
			]
		}
	d_n =
		{ zero
		& aie_instruction = ".n"
		, aie_arguments   = [A_OFFSET, LABEL]
		, aie_description = join " "
			[ "Indicates the arity of node entry labels."
			, "The label is the label of the corresponding descriptor, or `_` if it does not exist."
			, "\n\nThere are some special cases:\n\n"
			, "- An arity of `-1` is for tuple selectors;\n"
			, "- An arity of `-2` is for indirection nodes;\n"
			, "- An arity of `-3` is for record selectors of basic types;\n"
			, "- An arity of `-4` is for record selectors of non-basic types.\n\n"
			, "See also {{`.nu`}}."
			]
		}
	d_nu =
		{ zero
		& aie_instruction = ".nu"
		, aie_arguments   = [A_OFFSET, B_OFFSET, LABEL]
		, aie_description = join " "
			[ "Indicates the arity of node entry labels with arguments on the B-stack (otherwise, {{`.n`}} is used)."
			, "The first integer is the number of A-stack arguments; the second the number of B-stack arguments."
			, "The label is the label of the corresponding descriptor, or `_` if it does not exist."
			]
		}
	d_o =
		{ zero
		& aie_instruction = ".o"
		, aie_arguments   = [A_OFFSET, B_OFFSET, STRING_]
		, aie_description = join " "
			[ "Indicates how many stack elements are 'offered' by a called function after a {{`jsr`}} follows."
			, "It is also used at the start of subroutines to indicate how many stack elements are 'offered' to that subroutine."
			, "The first integer is the number of elements on the A-stack; the second that of B-stack elements."
			, "The optional third argument indicates the type of the B-stack elements, e.g. `bbi` for two booleans and an integer."
			]
		}
	d_r =
		{ zero
		& aie_instruction = ".r"
		, aie_arguments   = [A_OFFSET, B_OFFSET, STRING_]
		, aie_description = join " "
			[ "Indicates how many stack elements are 'returned' by the following subroutine."
			, "The first integer is the number of elements on the A-stack; the second that of B-stack elements."
			, "The optional third argument indicates the type of the B-stack elements, e.g. `bbi` for two booleans and an integer."
			]
		}

	d_export =
		{ zero
		& aie_instruction = ".export"
		, aie_arguments   = [LABEL]
		, aie_description = "Exports a label (allows linking)."
		}
	d_inline =
		{ zero
		& aie_instruction = ".inline"
		, aie_arguments   = [LABEL]
		, aie_description = "Indicates that a label can (should) be inlined (usually for performance reasons)."
		}

	d_module =
		{ zero
		& aie_instruction = ".module"
		, aie_arguments   = [LABEL, STRING, STRING_]
		, aie_description = join " "
			[ "Indicates the name of the module, and its label in the data segment."
			, "A final optional string stores the local date and time on which the module was last modified in a YYYYMMDDHHMMSS format."
			, "The Clean compiler only writes this string when `-wmt` is passed (which is the case with `clm` and `cpm`)."
			]
		}
	d_depend =
		{ zero
		& aie_instruction = ".depend"
		, aie_arguments   = [STRING, STRING_]
		, aie_description = join " "
			[ "Indicates a module that this module depends on."
			, "A second optional string indicates the local date and time on which the dependency was last modified in a YYYYMMDDHHMMSS format."
			, "The Clean compiler only writes this string when `-wmt` is passed (which is the case with `clm` and `cpm`)."
			]
		}
	d_end =
		{ zero
		& aie_instruction = ".end"
		, aie_description = "Indicates the end of the ABC file."
		}
	d_endinfo =
		{ zero
		& aie_instruction = ".endinfo"
		, aie_description = "Indicates the end of the metadata in the ABC file."
		}
	d_start =
		{ zero
		& aie_instruction = ".start"
		, aie_arguments   = [LABEL]
		, aie_description = "Indicates the label to start execution at."
		}

/**
 * Instructions without documentation yet
 */
other_instructions :: [String]
other_instructions =
	[ "add_args"
	, "addLU"
	, "buildF_b"
	, "buildhr"
	, "catS"
	, "call"
	, "cmpS"
	, "ceilingR"
	, "copy_graph"
	, "code_channelP"
	, "create_channel"
	, "currentP"
	, "del_args"
	, "divLU"
	, "divU"
	, "eqD_b"
	, "eq_symbol"
	, "exit_false"
	, "fill1"
	, "fill1_r"
	, "fillF_b"
	, "fillcaf"
	, "fillcp"
	, "fillcp_u"
	, "floordivI"
	, "getWL"
	, "get_desc_arity"
	, "get_desc_arity_offset"
	, "get_desc_flags_b"
	, "get_desc0_number"
	, "get_thunk_desc"
	, "gtU"
	, "in"
	, "is_record"
	, "ItoP"
	, "jmp_ap"
	, "jmp_ap_upd"
	, "jmp_i"
	, "jmp_upd"
	, "jmp_not_eqZ"
	, "jrsr"
	, "jsr_ap"
	, "jsr_i"
	, "load_module_name"
	, "ltU"
	, "modI"
	, "mulUUL"
	, "new_ext_reducer"
	, "new_int_reducer"
	, "newP"
	, "out"
	, "print"
	, "printD"
	, "print_char"
	, "print_int"
	, "print_real"
	, "print_r_arg"
	, "print_sc"
	, "print_symbol"
	, "print_symbol_sc"
	, "pushcaf"
	, "push_finalizers"
	, "pushA_a"
	, "pushD"
	, "pushD_a"
	, "pushF_a"
	, "pushL"
	, "pushLc"
	, "pushzs"
	, "push_arg"
	, "push_arg_b"
	, "push_args_u"
	, "push_array"
	, "push_arraysize"
	, "push_b_a"
	, "push_a_r_args"
	, "push_r_args"
	, "push_r_args_a"
	, "push_r_args_b"
	, "push_r_args_u"
	, "push_r_arg_D"
	, "push_r_arg_u"
	, "push_wl_args"
	, "pushZ"
	, "pushZR"
	, "putWL"
	, "randomP"
	, "release"
	, "remU"
	, "repl_arg"
	, "repl_args_b"
	, "repl_r_args"
	, "repl_r_args_a"
	, "send_graph"
	, "send_request"
	, "set_continue"
	, "set_defer"
	, "set_entry"
	, "set_finalizers"
	, "setwait"
	, "shiftrU"
	, "sincosR"
	, "sliceS"
	, "stop_reducer"
	, "subLU"
	, "addIo"
	, "mulIo"
	, "subIo"
	, "suspend"
	, "testcaf"
	, "truncateR"
	, "updateS"
	, ".algtype"
	, ".caf"
	, ".code"
	, ".comp"
	, ".a"
	, ".ai"
	, ".desc"
	, ".desc0"
	, ".descn"
	, ".descexp"
	, ".descs"
	, ".keep"
	, ".impdesc"
	, ".implab"
	, ".implib"
	, ".impmod"
	, ".impobj"
	, ".newlocallabel"
	, ".n_string"
	, ".pb"
	, ".pd"
	, ".pe"
	, ".pl"
	, ".pld"
	, ".pn"
	, ".pt"
	, ".record"
	, ".string"
	]
