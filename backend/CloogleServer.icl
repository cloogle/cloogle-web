module CloogleServer

/**
 * The main module for the Cloogle TCP server.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdArray
import StdBool
import StdFile
from StdFunc import id, o
import StdList
import StdMisc
import StdOrdList
import StdOverloaded
import StdOverloadedList
import StdString
import StdTuple

from TCPIP import :: IPAddress, :: Port, instance toString IPAddress

import Clean.Types
import Clean.Types.Parse
import Clean.Types.Unify
import Clean.Types.Util
import Control.Applicative
import Control.Monad
import Data.Error
from Data.Func import $, hyperstrict, mapSt, seqSt, on, `on`
import Data.Functor
from Data.List import intersperse, permutations
import Data.Maybe
import Data.Tuple
from System._Linux import errno
import System.CommandLine
import System.File
import System.Options
import System.Time
import qualified Text
from Text import class Text(concat,split,startsWith,toLowerCase), instance Text String, <+
import Text.GenJSON

import Cloogle.API
import Cloogle.DB
import Cloogle.Search
import Cloogle.Search.Rank

import Util.SimpleTCPServer
import Util.Cache
import Util.License
import Util.Memory

MAX_RESULTS        :== 15
CACHE_PREFETCH     :== 5
CACHE_NS_THRESHOLD :== 20000000

:: RequestCacheKey =
	{ c_unify            :: !?Type
	, c_name             :: !?String
	, c_exactName        :: !?String
	, c_className        :: !?String
	, c_typeName         :: !?String
	, c_using            :: !?[String]
	, c_modules          :: !?[String]
	, c_libraries        :: !?[String]
	, c_include_builtins :: !Bool
	, c_include_core     :: !Bool
	, c_page             :: !Int
	}

derive JSONEncode Kind, Type, RequestCacheKey, TypeContext, TypeRestriction,
	RankSettings
derive JSONDecode Kind, Type, RequestCacheKey, TypeContext, TypeRestriction,
	RankSettings, RankConstraint
instance toString RequestCacheKey
where toString rck = toString $ toJSON rck

toRequestCacheKey :: (String -> Bool) (Map Name [TypeDef]) !Request -> RequestCacheKey
toRequestCacheKey alwaysUnique allsyns r =
	{ c_unify            = snd <$>
		prepare_unification True alwaysUnique allsyns <$>
		(parseType o fromString =<< r.unify)
	, c_name             = toLowerCase <$> r.Request.name
	, c_exactName        = r.exactName
	, c_className        = r.className
	, c_typeName         = r.typeName
	, c_using            = r.using
	, c_modules          = sort <$> r.modules
	, c_libraries        = sort <$> r.libraries
	, c_include_builtins = fromMaybe DEFAULT_INCLUDE_BUILTINS r.include_builtins
	, c_include_core     = fromMaybe DEFAULT_INCLUDE_CORE r.include_core
	, c_page             = fromMaybe 0 r.page
	}
fromRequestCacheKey :: RequestCacheKey -> Request
fromRequestCacheKey k =
	{ unify            = concat <$> print False <$> unprepare <$> k.c_unify
	, name             = k.c_name
	, exactName        = k.c_exactName
	, className        = k.c_className
	, typeName         = k.c_typeName
	, using            = k.c_using
	, modules          = k.c_modules
	, libraries        = k.c_libraries
	, include_builtins = ?Just k.c_include_builtins
	, include_core     = ?Just k.c_include_core
	, page             = ?Just k.c_page
	}
where
	unprepare :: !Type -> Type
	unprepare (Type t ts) = Type t (map unprepare ts)
	unprepare (Func is t (TypeContext tc)) = Func (map unprepare is) (unprepare t) (TypeContext (map unprepareTR tc))
	unprepare (Var tv) = Var (tv % (1,size tv-1))
	unprepare (Cons v ts) = Cons (v % (1,size v-1)) (map unprepare ts)
	unprepare (Uniq t) = Uniq (unprepare t)
	unprepare (Forall ts t (TypeContext tc)) = Forall (map unprepare ts) (unprepare t) (TypeContext (map unprepareTR tc))
	unprepare (Arrow mt) = Arrow (unprepare <$> mt)
	unprepare (Strict t) = Strict (unprepare t)

	unprepareTR :: !TypeRestriction -> TypeRestriction
	unprepareTR (Instance c ts) = Instance c (map unprepare ts)
	unprepareTR (Derivation g t) = Derivation g (unprepare t)

:: Options =
	{ port                      :: !Int
	, db_file                   :: !FilePath
	, reload_cache              :: !Bool
	, rank_settings_constraints :: !?FilePath
	, test_file                 :: !?FilePath
	, test_options              :: ![TestOption]
	, license_options           :: !LicenseOptions
	, no_license                :: !Bool
	}

:: TestOption
	= TO_NoUnify
	| TO_Quiet

instance == TestOption
where
	(==) TO_NoUnify to = to=:TO_NoUnify
	(==) TO_Quiet   to = to=:TO_Quiet

instance zero Options
where
	zero =
		{ port                      = 31215
		, db_file                   = "db.jsonl"
		, reload_cache              = False
		, rank_settings_constraints = ?None
		, test_file                 = ?None
		, test_options              = []
		, license_options           = zero
		, no_license                = False
		}

optionDescription :: Option Options
optionDescription = WithHelp True $ Options
	[ Biject
		(\opts -> opts.license_options)
		(\opts licenseOptions -> {opts & license_options = licenseOptions})
		licenseOptions
	, Flag
		"--no-license"
		(\opts -> Ok {opts & no_license = True})
		"Do not show any license information"
	, Shorthand "-p" "--port" $ Option
		"--port"
		(\port opts -> case (toInt port, port) of
			(0, "0") -> Error ["Cannot use port 0"]
			(0, p)   -> Error ["'" <+ p <+ "' is not an integer"]
			(p, _)   -> Ok {Options | opts & port=p})
		"PORT"
		"Listen on port PORT (default: 31215)"
	, Option
		"--db"
		(\file opts -> Ok {opts & db_file=file})
		"FILE"
		"Use the database in FILE (default: db.jsonl)"
	, Flag
		"--reload-cache"
		(\opts -> Ok {opts & reload_cache=True})
		"Reload the cache in the background"
	, Option
		"--rank-settings-constraints"
		(\file opts -> Ok {opts & rank_settings_constraints = ?Just file})
		"FILE"
		"Output symbolic rank constraints in Z3 format based on test cases in FILE"
	, Option
		"--test"
		(\file opts -> Ok {opts & test_file = ?Just file})
		"FILE"
		"Load queries from FILE and execute them (do not start a TCP server)"
	, Flag
		"--test-no-unify"
		(\opts -> Ok {opts & test_options=[TO_NoUnify:opts.test_options]})
		"Do not test queries that require unification (only used with --test)"
	, Flag
		"--test-quiet"
		(\opts -> Ok {opts & test_options=[TO_Quiet:opts.test_options]})
		"Do not print test queries as they are executed (only used with --test)"
	]

Start w
# (prog,args,w) = case getCommandLine w of
	([prog:args],w) -> (prog,args,w)
	_               -> abort "getCommandLine returned 0 elements\n"
# opts = parseOptions optionDescription args zero
| isError opts
	# (io,w) = stdio w
	# io = io <<< 'Text'.join "\n" (fromError opts) <<< "\n"
	# (_,w) = fclose io w
	= w
# opts = fromOk opts
# w = if opts.no_license w (printLicenseDetails prog w)
# (stop,w) = handleLicenseOptions opts.license_options w
| stop
	= w
# w = disableSwap w
#! (_,f,w) = fopen opts.db_file FReadText w
#! (db,f) = openDB f
#! (ok,db) = isJustU db
| not ok = errexit "Could not open database\n" -1 w
#! (_,w) = fclose f w
#! db = hyperstrict (fromJust db)
| isJust opts.rank_settings_constraints = computeRankConstraints (fromJust opts.rank_settings_constraints) db w
#! (f,w) = readFile "rank_settings.json" w
# rsets = fromJSON $ fromString $ fromOk f
| isError f || isNone rsets = errexit "Could not open rank settings\n" -1 w
#! rsets = fromJust rsets
#! (ok,rsets) = setRankSettings rsets
| not ok = errexit "Failed to set rank settings\n" -1 w
| isJust opts.test_file
	# (ok,f,w) = fopen (fromJust opts.test_file) FReadText w
	| not ok = errexit "Could not open test file\n" -1 w
	= test opts.test_options f db w
| opts.reload_cache
	= reloadCache db w
= serve
	{ handler           = handle
	, logger            = ?Just log
	, port              = opts.Options.port
	, connect_timeout   = ?Just 3600000 // 1h
	, keepalive_timeout = ?Just 5000    // 5s
	} db w
where
	disableSwap :: *World -> *World
	disableSwap w
	# (ok,w) = mlockall (MCL_CURRENT bitor MCL_FUTURE) w
	| ok = w
	# (err,w) = errno w
	= snd $ fclose (stderr <<< "Could not lock memory (" <<< err <<< "); process may get swapped out\n") w

errexit :: !String !Int !*World -> *World
errexit msg rcode w
# (_,w) = fclose (stderr <<< msg) w
= setReturnCode rcode w

handle :: !(?Request) !*CloogleDB !*World -> *(!Response, !(!?CacheKey, !MicroSeconds), !*CloogleDB, !*World)
handle ?None db w = (err InvalidInput "Couldn't parse input" ?None, (?None,0), db, w)
handle (?Just request=:{unify,name,page}) db w
	#! (start,w) = nsTime w
	//Check cache
	#! (alwaysUnique,db) = alwaysUniquePredicate db
	#! (allTypeSyns,db) = allTypeSynonyms db
	#! key = toRequestCacheKey alwaysUnique allTypeSyns request
	#! (mbResponse, w) = readCache key w
	| isJust mbResponse
		# r = fromJust mbResponse
		= respond start ?None {r & return = if (r.return == 0) 1 r.return} db w
	| isJust name && size (fromJust name) > 256
		= respond start ?None (err InvalidName "Function name too long" ?None) db w
	| isJust unify && isNone (parseType $ fromString $ fromJust unify)
		= respond start ?None (err InvalidType "Couldn't parse type" ?None) db w
	| all isNone [unify,name,request.exactName,request.typeName,request.className] && isNone request.using
		= respond start ?None (err InvalidInput "Empty query" ?None) db w
	#! (warnings,db) = getWarnings (parseType =<< fromString <$> unify) db
	// Results
	#! drop_n = fromJust (page <|> pure 0) * MAX_RESULTS
	#! (mbRes,suggs,db) = searchWithSuggestions request db
	   res = fromOk mbRes
	| isError mbRes = respond start ?None (err ServerError (fromError mbRes) ?None) db w
	#! suggs = if (isEmpty suggs) ?None (?Just suggs)
	#! results = [r \\ r <|- Drop drop_n res]
	#! more = max 0 (length results - MAX_RESULTS)
	// Suggestions
	#! w = seqSt
		(\(req,res) w -> cachePages (toRequestCacheKey alwaysUnique allTypeSyns req) CACHE_PREFETCH 0 zero res w)
		(fromMaybe [] suggs)
		w
	#! suggs = sortBy ((<) `on` snd) <$> map (appSnd length) <$> suggs
	#! (results,nextpages) = splitAt MAX_RESULTS results
	// Response
	#! response = if (isEmpty results)
		(err NoResults "No results" suggs)
		{ zero
		& data           = results
		, more_available = ?Just more
		, suggestions    = suggs
		, warnings       = if (isEmpty warnings) ?None (?Just $ removeDup warnings)
		}
	// Save page prefetches
	#! w = cachePages key CACHE_PREFETCH 1 response nextpages w
	// Save cache file
	= respond start (?Just key) response db w
where
	respond :: !Timespec !(?RequestCacheKey) !Response !*CloogleDB !*World ->
		*(!Response, !(!?CacheKey, !MicroSeconds), !*CloogleDB, !*World)
	respond start key r db w
	#! (end,w) = nsTime w
	#! duration = 1000000000 * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec
	#! cache = duration > CACHE_NS_THRESHOLD
	= (r, (if cache (cacheKey <$> key) ?None, duration / 1000), db, case (cache,key) of
		(True,?Just k) -> writeCache LongTerm k r w
		_              -> w)

	getWarnings :: !(?Type) !*CloogleDB -> (![String], !*CloogleDB)
	getWarnings ?None db = ([], db)
	getWarnings (?Just t) db
	# (warnings,db) = mapSt checkTypeVariables [(n,length args) \\ type=:(Type n args) <- subtypes t] db
	= (catMaybes warnings,db)
	where
		checkTypeVariables :: !(!Name,!Int) !*CloogleDB -> *(!?String, !*CloogleDB)
		checkTypeVariables (name,nargs) db
		# (entries,db) = getExactNameMatches readableName` db
		# nvars = sort $ removeDup [length tde_typedef.td_args \\ TypeDefEntry {tde_typedef} <|- entries]
		| isEmpty nvars
			= (?None,db)
		| isMember nargs nvars
			= (?None,db)
			= case nvars of
				[1] -> (?Just (concat $ warn ++ ["1 was expected"]),db)
				[n] -> (?Just (concat $ warn ++ [toString n," were expected"]),db)
				_   -> (?Just (concat $ warn ++ intersperse ", " (map toString (init nvars)) ++ [", or ",toString (last nvars)," were expected"]),db)
		where
			warn = ["Type {{`",readableName`,"`}} used with ",toString nargs," argument",if (nargs==1) "" "s"," where "]

			readableName` = case name of
				s | s.[0]<>'_' -> name
				"_List"        -> "[]"
				"_List!"       -> "[ !]"
				"_!List"       -> "[!]"
				"_!List!"      -> "[!!]"
				"_#List"       -> "[#]"
				"_#List!"      -> "[#!]"
				"_Maybe"       -> "?^"
				"_!Maybe"      -> "?"
				"_#Maybe"      -> "?#"
				"_Array"       -> "{}"
				"_!Array"      -> "{!}"
				"_#Array"      -> "{#}"
				"_32#Array"    -> "{32#}"
				s
					| s % (1,5) == "Tuple"
						# n = toInt (s % (6, size s-1))
						| n > 0
							-> {createArray (n+1) ',' & [0]='(', [n]=')'}
							-> s
						-> s

	cachePages :: !RequestCacheKey !Int !Int !Response ![Result] !*World -> *World
	cachePages key _ _  _ [] w = w
	cachePages key 0 _  _ _  w = w
	cachePages key npages i response results w
	# w = writeCache Brief req` resp` w
	= cachePages key (npages - 1) (i + 1) response keep w
	where
		req` = { key & c_page = key.c_page + i }
		resp` =
			{ response
			& more_available = ?Just $ max 0 (length results - MAX_RESULTS)
			, data = give
			}
		(give,keep) = splitAt MAX_RESULTS results

reloadCache :: !*CloogleDB !*World -> *World
reloadCache db w
# (ks,w) = allCacheKeys LongTerm w
= loop ks db w
where
	loop :: ![RequestCacheKey] !*CloogleDB !*World -> *World
	loop [] _ w = w
	loop [k:ks] db w
	# w = removeFromCache LongTerm k w
	# (_,_,db,w) = handle (?Just $ fromRequestCacheKey k) db w
	# db = resetDB db
	= loop ks db w

test :: ![TestOption] !*File !*CloogleDB !*World -> *World
test opts queries db w
# (e,queries) = fend queries
| e = w
# (qstring,queries) = freadline queries
# qstring = {c \\ c <-: qstring | c <> '\n' && c <> '\r'}
# q = parseSingleLineRequest qstring
| isError q
	# w = snd $ fclose (stderr <<< "Warning: could not parse '" <<< qstring <<< "'; " <<< fromError q <<< "\n") w
	= test opts queries db w
# q = fromOk q
| excluded q
	= test opts queries db w
# w = if (isMember TO_Quiet opts)
	w
	(snd $ fclose (stderr <<< qstring) w)
#! (Clock start,w) = clock w
#! (resp,_,db,w) = handle (?Just q) db w
| (hyperstrict resp).return < 0 = abort "return code was < 0\n"
#! (Clock end,w) = clock w
# w = if (isMember TO_Quiet opts)
	w
	(snd $ fclose (stderr <<< "\t" <<< (end-start) <<< "\n") w)
= test opts queries db w
where
	excluded :: !Request -> Bool
	excluded r = isJust r.unify && isMember TO_NoUnify opts

computeRankConstraints :: !FilePath !*CloogleDB !*World -> *World
computeRankConstraints constraintfile db w
#! (f,w) = readFile constraintfile w
# constraints = fromJSON $ fromString $ fromOk f
| isError f || isNone constraints = errexit "Could not open rank settings constraints\n" -1 w
# constraints = fromJust constraints
# (settings,db,w) = findRankSettings constraints db w
| isError settings = errexit (fromError settings +++ "\n") -1 w
# settings = fromOk settings
# (io,w) = stdio w
# io = io <<< jsonPrettyPrint (toJSON settings) <<< "\n"
# (_,w) = fclose io w
= w

:: LogMemory =
	{ mem_ip         :: IPAddress
	, mem_time_start :: Tm
	, mem_time_end   :: Tm
	, mem_request    :: ?Request
	}

instance zero LogMemory
where
	zero =
		{ mem_ip         = undef
		, mem_time_start = undef
		, mem_time_end   = undef
		, mem_request    = undef
		}

:: MicroSeconds :== Int

:: LogMessage` :== LogMessage (?Request) Response (?CacheKey, MicroSeconds)

:: LogEntry =
	{ ip            :: String
	, time_start    :: (String, Int)
	, time_end      :: (String, Int)
	, microseconds  :: Int
	, request       :: ?Request
	, cachekey      :: ?String
	, response_code :: Int
	, results       :: Int
	}

derive JSONEncode LogEntry

log :: LogMessage` (?LogMemory) *World -> *(?LogMemory, *World)
log msg mem w
# mem     = fromJust (mem <|> pure zero)
# (mem,w) = updateMemory msg mem w
| not needslog = (?Just mem, w)
# (io,w)  = stdio w
# io      = io <<< toString (toJSON $ makeLogEntry msg mem) <<< "\n"
= (?Just mem, snd (fclose io w))
where
	needslog = case msg of (Sent _ _) = True; _ = False

	updateMemory :: LogMessage` LogMemory *World -> *(LogMemory, *World)
	updateMemory (Connected ip) s w = ({s & mem_ip=ip}, w)
	updateMemory (Received r)   s w
	# (t,w) = localTime w
	= ({s & mem_time_start=t, mem_request=r}, w)
	updateMemory (Sent _ _)     s w
	# (t,w) = localTime w
	= ({s & mem_time_end=t}, w)
	updateMemory _                   s w = (s,w)

	makeLogEntry :: LogMessage` LogMemory -> LogEntry
	makeLogEntry (Sent response (ck,us)) mem =
		{ ip            = toString mem.mem_ip
		, time_start    = (toString mem.mem_time_start, toInt $ timeGm mem.mem_time_start)
		, time_end      = (toString mem.mem_time_end, toInt $ timeGm mem.mem_time_end)
		, microseconds  = us
		, request       = mem.mem_request
		, cachekey      = ck
		, response_code = response.return
		, results       = length response.data
		}
	makeLogEntry _ _ = abort "CloogleServer: failure in makeLogEntry\n"

err :: !CloogleError !String !(?[(Request,Int)]) -> Response
err c m suggs =
	{ zero
	& return         = toInt c
	, msg            = m
	, suggestions    = suggs
	}
