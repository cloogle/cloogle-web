implementation module Util.Cache

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdFile
from StdFunc import seqList, :: St
import StdFunctions
import StdList
import StdOrdList
import StdString
import StdTuple

import Control.Applicative
import Control.Monad
import Crypto.Hash.MD5
import Data.Error
from Data.Func import $, on
import Data.Functor
import Data.Maybe
import Data.Tuple
import System.Directory
import System.File
import System.FilePath
import System.SysCall
import System.Time
from Text import class Text(endsWith), instance Text String
import Text.GenJSON

cache_types :== [Brief, LongTerm]

typeToDir :: !CacheType -> FilePath
typeToDir LongTerm = "lt"
typeToDir Brief = "brief"

cache_dir :: !CacheType -> FilePath
cache_dir t = "." </> "cache" </> typeToDir t

cacheKey :: (a -> CacheKey) | toString a
cacheKey = md5 o toString

toCacheFile :: !CacheType -> a -> FilePath | toString a
toCacheFile t = (</>) (cache_dir t) o cacheKey

readCache :: !a !*World -> (! ?b, !*World) | toString a & JSONDecode{|*|} b
readCache k w
# (files,w) = seqList [appFst error2mb o readFile (toCacheFile t k) \\ t <- cache_types] w
= (join $ fromJSON <$> fromString <$> foldl (<|>) empty files, w)

allCacheKeys :: !CacheType !*World -> (![a], !*World) | JSONDecode{|*|} a
allCacheKeys t w
# (fps,w) = appFst (fmap (map ((</>) (cache_dir t)) o filter (endsWith ".key")))
	$ readDirectory (cache_dir t) w
| isError fps = ([], w)
# (infos,w) = appFst catMaybes $ seqList
	[appFst (fmap (tuple f) o error2mb) o getFileInfo f \\ f <- fromOk fps] w
# infos = sortByAccessTime infos
# (files,w) = seqList [appFst error2mb o readFile f \\ (f,_) <- infos] w
= (catMaybes $ catMaybes $ map (fmap (fromJSON o fromString)) files, w)
where
	sortByAccessTime = sortBy (on (<) (\(_,i)->i.lastAccessedTime))

instance < Tm where (<) a b = timeGm a < timeGm b

writeCache :: !CacheType !a !b !*World -> *World | toString, JSONEncode{|*|} a & JSONEncode{|*|} b
writeCache t k v w
# (_,w) = writeFile file (toString $ toJSON v) w
# (_,w) = writeFile (file +++ ".key") (toString $ toJSON k) w
= w
where
	file = toCacheFile t k

removeFromCache :: !CacheType !a -> *World -> *World | toString a
removeFromCache t k =
	snd o deleteFile (cache_dir t </> cacheKey k +++ ".key") o
	snd o deleteFile (cache_dir t </> cacheKey k)
