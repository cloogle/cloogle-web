implementation module Util.SimpleTCPServer

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Data.Maybe
import System._Posix

import TCPIP

instance zero (Logger a b s t) where zero = \_ _ w -> (undef, w)

serve :: !(Server req res .st logst sentinfo) .st !*World -> *World | fromString req & toString res
serve server st w
# (ok, mbListener, w) = openTCP_Listener server.port w
| not ok = abort ("Couldn't open port " +++ toString server.port +++ "\n")
# listener = fromJust mbListener
# (_,w) = signal 17 1 w // SIGCHLD, SIG_IGN: no notification if child ps dies
# (listener, w) = loop listener w
= closeRChannel listener w
where
	logger = fromMaybe zero server.logger

	loop :: TCP_Listener *World -> (TCP_Listener, *World)
	loop li w
	#! (tRep,conn,li,w) = receive_MT server.connect_timeout li w
	| tRep <> TR_Success
		= (li,w)
	#! (ip,{rChannel,sChannel}) = fromJust conn
	#! (pid,w) = fork w
	| pid < 0
		= abort "fork failed\n"
	| pid > 0 // Parent: handle new requests
		# w = abortConnection sChannel (closeRChannel rChannel w)
		= loop li w
	| otherwise // Child: handle current request
		# w = closeRChannel li w
		#! (logst,w) = logger (Connected ip) ?None w
		= (undef, handle logst st rChannel sChannel w)

	handle logst st rChannel sChannel w
	#! (tRep,msg,rChannel,w) = receive_MT server.keepalive_timeout rChannel w
	| tRep <> TR_Success
		#! (logst,w)         = logger Disconnected logst w
		#! w                 = closeChannel sChannel (closeRChannel rChannel w)
		= exit 0 w
	#! msg                   = fromString (toString (fromJust msg))
	#! (logst, w)            = logger (Received msg) logst w
	#! (resp, hidden, st, w) = server.handler msg st w
	#! (sChannel, w)         = send (toByteSeq (toString resp)) sChannel w
	#! (logst, w)            = logger (Sent resp hidden) logst w
	= handle logst st rChannel sChannel w

signal :: !Int !Int !*World -> *(!Int, !*World)
signal signum handler w = code {
	ccall signal "II:I:A"
}
