module builtin_syntax

/**
 * Test program to check for some common errors in Builtin.Syntax.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Clean.Types
import Clean.Types.Parse
import Data.Error
from Data.Func import mapSt, seqSt
import System.CommandLine
import System.Directory
import System.File
import System.FilePath
import System.Process
import Text

from Cloogle.API import :: SyntaxExample{..}
import Cloogle.DB
import Builtin.Syntax

DOT_SUCCESS :== "\033[0;32m.\033[0m"
DOT_FAILURE :== "\033[0;31mF\033[0m"
DOT_SKIPPED :== "\033[0;33ms\033[0m"

MSG_SUCCESS :== "\033[0;32m OK\033[0m"
MSG_FAILURE :== "\033[0;31m failed!\033[0m"

DIRECTORY :== "examples"

CLMFLAGS =:
	[ "-c"
	, "-dynamics"
	, "-I", "nitrile-packages/linux-x64/base-lib/lib"
	, "-I", "nitrile-packages/linux-x64/base-stdenv/lib"
	, "-I", "nitrile-packages/linux-x64/category-theory/lib"
	, "-I", "nitrile-packages/linux-x64/containers/lib"
	, "-I", "nitrile-packages/linux-x64/json/lib"
	, "-I", DIRECTORY
	]

Start w
# (_,w) = ensureDirectoryExists DIRECTORY w
# (_,failed,w) = seqSt test builtin_syntax (0,0,w)
= case failed of
	0
		# (_,w) = recursiveDelete DIRECTORY w
		-> w
	_
		# (_,w) = fclose
			(stderr
				<<< "Some tests failed. If test numbers are given, you can check the errors with:\n"
				<<< "\tcd " <<< DIRECTORY <<< "\n"
				<<< "\tclm " <<< join " " CLMFLAGS <<< " _example<number>\n")
			w
		-> setReturnCode -1 w

test :: !SyntaxEntry !*(!Int, !Int, !*World) -> *(!Int, !Int, !*World)
test se (i,failed,w)
# err = stderr <<< "Checking syntax for '" <<< se.syntax_title <<< "': "
# (_,w) = fclose err w
# (results,(j,w)) = mapSt test_example se.syntax_examples (i,w)
# err = stderr
	<<< if (and results)
		MSG_SUCCESS
		(MSG_FAILURE +++ " (" +++ join ", " [toString (n+i) \\ n <- [0..] & False <- results] +++ ")")
	<<< "\n"
# (_,w) = fclose err w

= (j,failed + length [f \\ f=:False <- results],w)

test_example :: !SyntaxExample !*(!Int, !*World) -> *(!Bool, !*(!Int, !*World))
test_example {example,cleanjs_start,bootstrap,requires_itask_compiler} (i,w)
| indexOf "not allowed" (toLowerCase example) >= 0 // examples that are marked as not allowed by the compiler
		|| startsWith "definition module " example // module headings
		|| startsWith "implementation module " example
		|| startsWith "system module " example
		|| startsWith "module " example
	# (_,w) = fclose (stderr <<< DOT_SKIPPED) w
	= (True, (i+1,w))
# dcl = join "\n"
	[ "definition module _example" <+ i
	: [b \\ b <- bootstrap | startsWith "import " b]
	++ case split " special " example of
		[_]
			| indexOf "(:==" example > 0
				-> [example] // abstract synonym type
			| indexOf "(=:" example > 0
				-> [example] // abstract newtype
				-> [l \\ l <- split "\n" example
						| startsWith "derive " l && indexOf " with " (trimComment l) > 0
							|| startsWith "generic " l]
		[_:_]
			-> [example]
	]
# icl = join "\n"
	[ "implementation module _example" <+ i
	: bootstrap
	++ case cleanjs_start of
		?None            -> complete_icl example
		?Just "macro"    -> complete_icl example
		?Just "rhs"      -> ["Start = " +++ example]
		?Just "macrorhs" -> ["Start = " +++ example]
		?Just s          -> ["unknown cleanjs_start " +++ s]
	]

# (_,w) = writeFile (DIRECTORY </> "_example" <+ i <+ ".dcl") dcl w
# (_,w) = writeFile (DIRECTORY </> "_example" <+ i <+ ".icl") icl w

# args = CLMFLAGS ++ if requires_itask_compiler ["-clc","cocl-itasks"] [] ++ ["_example" <+ i]
# (Ok (h,_),w) = runProcessIO "clm" args ?None w
# (Ok r,w) = waitForProcess h w
# ok = r == 0

# (_,w) = fclose (stderr <<< if ok DOT_SUCCESS DOT_FAILURE) w
= (ok,(i+1,w))
where
	complete_icl :: !String -> [String]
	complete_icl example
	| startsWith "foreign export " example =
		[ name +++ " :: Int"
		, name +++ " = 0"
		] with name = example % (15, indexOfAfter 15 " " example)
	| indexOf "(:==" example > 0 = // abstract synonym type
		[replaceSubString "(:==" ":==" (example % (0,size example-2))]
	| indexOf "(=:" example > 0 = // abstract newtype
		[replaceSubString "(=:" "=:" (example % (0,size example-2))]
	# example = hd (split "special" example)
	| example.[0] == '(' && indexOf ") =" example > 0 = // pattern
		[ "tempfun = " +++ (example % (0,indexOf ") =" example + 1))
		, "where"
		, "\t" +++ replaceSubString "\n" "\n\t" example
		]
	# lines = split "\n" example
	| length lines == 1
		# line = trimComment (hd lines)
		# doublecolon = indexOf "::" line
		| doublecolon > 0
				&& not (startsWith "generic " line || startsWith "class " line || startsWith "from " line)
			// function without implementation
			= case parseType [c \\ c <-: line & i <- [0..] | i > doublecolon+2] of
				?Just (Func is _ _) ->
					[ line
					, line % (0,indexOf " " line) +++ join " " ["_" \\ _ <- is] +++ " = undef"
					, "from StdMisc import undef"
					]
				_ ->
					[ line
					, line % (0,indexOf " " line) +++ " = undef"
					, "from StdMisc import undef"
					]
		= lines
	= [l \\ l <- lines | not (startsWith "derive " l && indexOf " with " (trimComment l) > 0)]

trimComment :: !String -> String
trimComment line = case indexOf "//" line of
	-1 -> line
	i  -> trim (line % (0, i-1))
