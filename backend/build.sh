#!/bin/bash

# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

set -ev
set -o pipefail

shopt -s globstar

PACKAGES="file git jq python3 z3"
apt-get update -qq
apt-get install -qq $PACKAGES --no-install-recommends

cd ..

nitrile update

# Build CloogleServer
rm -rf nitrile-packages
rm -f **/*.abc
nitrile --arch=x86 fetch
nitrile --arch=x86 build

# Build builddb
rm -rf nitrile-packages
rm -f **/*.abc
nitrile fetch
nitrile build

./util/fetch_libs.sh /opt/clean/lib

# Build common problems index
git clone https://gitlab.com/cloogle/common-problems.git /tmp/common-problems
(cd /tmp/common-problems; ./build_index.py)

cd backend
./builddb --no-license --common-problems /tmp/common-problems/common-problems.json > db.jsonl
./CloogleServer --no-license --rank-settings-constraints rank_settings_constraints.json | tee rank_settings.json
