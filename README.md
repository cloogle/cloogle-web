# cloogle-web

This is a web frontend for [Cloogle][], a search engine for [Clean][]. With
Cloogle you can:

:mag: [Search](https://cloogle.org/#how%20to%20use) for functions, types,
classes, and modules from Clean libraries  
:bookmark: [Search](https://cloogle.org/#how%20to%20use) for language
documentation, common compiler errors, and ABC instructions  
:green_book: Read the [language report](https://cloogle.org/doc) online  
:open_file_folder: Easily [browse](https://cloogle.org/src) the source code of
all indexed libraries  
:chart_with_upwards_trend: View long-term
[usage statistics](https://cloogle.org/stats)

[Use Cloogle online!][cloogle.org]

Cloogle was inspired by [Hoogle][]. As of June 27, 2018, Cloogle indexes
100&times; less lines of code than Hoogle (25 thousand vs. 2.3 million). This
allows Cloogle to implement more advanced features, such as `using` queries to
find usages.

---

**Readme contents**

[[_TOC_]]

---

## Interfacing with Cloogle
The recommended way to use Cloogle is using the web interface. You can also
interface with the TCP or HTTP (preferred) backend directly. For this, you can
use the Clean library [libcloogle][] or the Python library [cloogle.py][]
(unmaintained). A brief description of the endpoints follows.

**TCP API**  
`CloogleServer` is a TCP server listening on port 31215 (typically). Send a
JSON request in the format described by the `Request` type in [Cloogle.API][],
followed by a newline character. The response is in the format described by the
`Response` type. The connection is kept alive for some time to allow further
requests.

To interface with Cloogle, it is recommended that you use the HTTP API rather
than the TCP API.

**HTTP API**  
The HTTP API is a simple wrapper around the TCP API, with a more stable API.
`api.php` should be called with a `GET` request where the `str` parameter
contains the search string. You may also add `mod` (a comma-separated list of
modules to search in) and `page` (for pagination: 0 for the first *n* results,
1 for the next *n*, etc.). For the query syntax in the `str` parameter, see the
'How to use' guidelines on the front page. The API returns the same JSON as the
TCP API.

**Adding a new frontend / client to the statistics**  
If you have created a new Cloogle client, please give it a specific user agent
and add it to the `$user_agents` array in `frontend/stats/ajax/conf.php`. That
way, it will show up in the statistics on the `/stats` endpoint. There are no
strict naming conventions, but you can have a look at the other user agents for
inspiration.

Each entry has a key, which is the name of the client (e.g. 'vim-clean'). The
value is an array with a required `pattern`, which is used in SQL `LIKE`
queries so you can use `%` as in `%Linux%` - in most cases, the pattern will be
equal to the name. The optional `url` is a link to where the client may be used
or downloaded.

## Local setup
**Requirements**  
- 2GB of disk space for the source code and docker images.
- 1GB of RAM to build the index and 500MB while running.
- A high-speed CPU; instruction throughput is the primary bottleneck.

**Instructions**  
After installing [docker-compose][] run the following commands:

```bash
cp .env.example .env
touch cloogle.log
sudo docker-compose up
```

Your Cloogle server now runs at port `31215` on your local machine.
The web frontend is available at port `80`.

The server can be configured in the file `.env`. Some options are described
below.

**Statistics**  
Statistics are kept in a MySQL database. The easiest way to enable this is to
use `docker-compose --profile with-statistics`, which creates a dedicated
container with a MariaDB instance.

Alternatively, create a new database with `db/install.sql` elsewhere and set
the `CLOOGLE_DB_*` configuration values in `.env` to use this database instead.
A separate database is required if you run multiple instance and want them to
share statistics.

**Garbage collection**  
By default garbage collection for the query cache is disabled. To enable it,
use `docker-compose --profile with-garbage-collection`.

**Speeding up caching with `tmpfs`**  
To speed up caching it is wise to use a `tmpfs` for the `cache` directory. This
can be achieved by adding the following line to `/etc/fstab`:

```
tmpfs /path/to/cloogle-web/cache tmpfs defaults,atime,nosuid,nodev,mode=1666,size=100m 0 0
```

**Automatic updating**  
To update a Cloogle instance automatically, a cron job can be used, for
example:

```cron
0 3 * * * cd /path/to/cloogle-web; ./util/update.sh --no-interactive --no-clear-cache --mail-failure EMAIL_ADDRESS --release-directory /PATH/TO/RELEASES &>> /var/log/cloogle-build.log
```

See `./util/update.sh --help` for details about the options.

**Using nginx as a proxy**  
If you intend to run this on a server that has port 80 occupied already, you
can use nginx as a proxy. This is also needed if you want to set up multiple
instances on one server and use nginx to route requests to either instance
depending on the hostname or path.

Set `CLOOGLE_FRONTEND_PORT` to `127.0.0.1:31280` in `.env` and use the
following nginx config:

```nginx
server {
	listen [::]:80;
	server_name example.org;

	location / {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}
}
```

If you set up HTTPS on the proxy server, `/api.php` should still be accessible
on port 80 for `request` in Cloogle.Client to work. An example configuration
with HTTPS support:

```nginx
server {
	listen [::]:80;
	server_name example.org;

	location /api.php {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}

	location / {
		return 301 https://example.org$request_uri;
	}
}

server {
	listen [::]:443 ssl;
	server_name example.org;

	include /etc/nginx/confsnippets/ssl.conf; # Common SSL settings
	ssl_certificate /etc/letsencrypt/live/example.org/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/example.org/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}
}
```

## Authors &amp; license
Cloogle is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Mart Lubbers
- Erin van der Veen
- Koen Dercksen
- Steffen Michels
- Gijs Alberts

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

The [logo](/frontend/logo.svg) was created by Erin van der Veen and is licensed
under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Cloogle]: https://gitlab.com/cloogle/cloogle
[Cloogle.API]: https://gitlab.com/cloogle/libcloogle/blob/main/src/Cloogle/API.dcl
[Cloogle.API (icl)]: https://gitlab.com/cloogle/libcloogle/blob/main/src/Cloogle/API.icl
[cloogle.org]: http://cloogle.org
[cloogle.py]: https://gitlab.com/cloogle-archived/cloogle.py
[docker-compose]: https://www.docker.com/products/docker-compose
[Hoogle]: https://github.com/ndmitchell/hoogle
[libcloogle]: https://gitlab.com/cloogle/libcloogle
