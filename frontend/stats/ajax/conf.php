<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

if (file_exists('../../conf.php'))
	require_once('../../conf.php');

$db = new mysqli(
	CLOOGLE_DB_HOST, CLOOGLE_DB_USER, CLOOGLE_DB_PASS, CLOOGLE_DB_NAME);
if (mysqli_connect_errno())
	return;

define('SQL_NOT_SILLYUSER', '`responsecode` not in (' . E_DOSPROTECT . ', ' . E_QUERYTOOLONG . ')');
define('SQL_SERVER_ERROR', E_CLOOGLEDOWN . ',' . E_TIMEOUT);

$db->query("SET time_zone = '+00:00'");
date_default_timezone_set('UTC');

$callback = $_GET['callback'];
if (!preg_match('/^[a-zA-Z0-9_]+$/', $callback))
	die('Invalid callback name');

$start = @$_GET['start'];
if ($start && !preg_match('/^\d+$/', $start))
	die ("Invalid start parameter: $start");

if ($start == 0) {
	$stmt = $db->stmt_init();
	$stmt->prepare("SELECT unix_timestamp(min(`date`)) as mindate FROM `log`");
	$stmt->execute();
	$stmt->bind_result($start);
	$stmt->fetch();
	$stmt->close();
}

$end = @$_GET['end'];
if ($end && !preg_match('/^\d+$/', $end))
	die ("Invalid end parameter: $end");

if (!$end || $end > time())
	$end = time();

$startTime = gmdate('Y-m-d H:M:S', $start);
$endTime = gmdate('Y-m-d H:M:S', $end);

$user_agents =
	[ 'Linux'          => ['pattern' => '%Linux%']
	, 'Macintosh'      => ['pattern' => '%Macintosh%']
	, 'Windows'        => ['pattern' => '%Windows%']
	, 'CloogleBot'     => ['pattern' => 'CloogleBot',     'url' => 'https://telegram.me/CloogleBot']
	, 'vim-clean'      => ['pattern' => 'vim-clean',      'url' => 'https://gitlab.com/clean-and-itasks/vim-clean']
	, 'cloogle-cli'    => ['pattern' => 'cloogle-cli',    'url' => 'https://gitlab.com/cloogle/cloogle-cli']
	, 'CloogleMail'    => ['pattern' => 'CloogleMail',    'url' => 'mailto:query@cloogle.org']
	, 'cloogle-irc'    => ['pattern' => 'cloogle-irc',    'url' => 'https://gitlab.science.ru.nl/cloogle/cloogle-irc']
	, 'CloogleDiscord' => ['pattern' => 'CloogleDiscord', 'url' => 'https://github.com/ErinvanderVeen/cloogle-discord']
	, 'clean-vscode'   => ['pattern' => 'clean-vscode',   'url' => 'https://github.com/W95Psp/CleanForVSCode']
	];
