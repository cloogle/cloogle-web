<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('./conf.php');

$sql =
	"SELECT dayofweek(`date`), hour(`date`), count(*) FROM `log`
	WHERE
		" . SQL_NOT_SILLYUSER . " AND
		`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
	GROUP BY dayofweek(`date`), hour(`date`)
	ORDER BY dayofweek(`date`) asc, hour(`date`) ASC";

$stmt = $db->stmt_init();
if (!$stmt->prepare($sql))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($day, $hour, $querycount);
$_results = [];

while ($stmt->fetch())
	$_results[$day - 1][$hour] = $querycount;

$stmt->close();

$results = [];
for ($d = 0; $d <= 6; $d++)
	for ($h = 0; $h <= 23; $h++)
		if (isset($_results[$d][$h]))
			$results[] = "[$h,$d," . $_results[$d][$h] . "]";
		else
			$results[] = "[$h,$d,null]";

header('Content-Type: text/javascript');
echo "$callback([" . join(",", $results) . "]);";
