<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

define('CLOOGLE_DB_HOST', $_ENV['CLOOGLE_DB_HOST']);
define('CLOOGLE_DB_USER', $_ENV['CLOOGLE_DB_USER']);
define('CLOOGLE_DB_PASS', $_ENV['CLOOGLE_DB_PASS']);
define('CLOOGLE_DB_NAME', $_ENV['CLOOGLE_DB_NAME']);

/* Only the error codes used by the frontend are listed here. For all error
 * codes, see the CloogleError type in libcloogle. */
define('E_NORESULTS', 127);
define('E_SERVERERROR', 131);

define('E_CLOOGLEDOWN', 150);
define('E_ILLEGALMETHOD', 151);
define('E_ILLEGALREQUEST', 152);
define('E_TIMEOUT', 153);
define('E_DOSPROTECT', 154); /* not returned any more but still here for statistics */
define('E_QUERYTOOLONG', 155);

define('CLEANHOME', '/opt/clean');
define('CLEANLIB', CLEANHOME . '/lib');

function containsModules($dir) {
	try {
		$d = new DirectoryIterator($dir);
		foreach ($d as $f) {
			if ($f->isDot())
				continue;
			if ($f->isDir() && containsModules($dir . '/' . $f->getFilename()))
				return true;
			else if ($f->isFile() && $f->getExtension() == 'icl')
				return true;
		}
	} catch (Exception $e) { }
	return false;
}

function get_index() {
	$index = json_decode(file_get_contents(CLEANLIB . '/index.json'), true);
	$index = array_filter($index, function($lib) { return containsModules(CLEANLIB . '/' . $lib['name']); });
	usort($index, function($x,$y) { return strcmp($x['name'], $y['name']); });
	return $index;
}
