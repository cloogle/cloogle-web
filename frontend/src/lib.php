<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('../conf.php');

function getDirsAndModules($dir) {
	global $ignored_dirs;
	$ds = [];
	$ms = [];

	try {
		$d = new DirectoryIterator($dir);
		foreach ($d as $f) {
			if ($f->isDot())
				continue;
			if ($f->isDir() && containsModules($dir . '/' . $f->getFilename()))
				$ds[] = $f->getFilename();
			else if ($f->getExtension() == 'icl')
				$ms[] = $f->getBasename('.icl');
		}

		natcasesort($ds);
		natcasesort($ms);
	} catch (Exception $e) {
		echo 'Failed to get directory ' . $dir;
	}

	return ['dirs' => $ds, 'modules' => $ms];
}

function makeBrowser($dir, $basemodule) {
	echo '<div class="browser togglee">';
	$elems = getDirsAndModules($dir);

	foreach ($elems['dirs'] as $d) {
		echo '<div class="browser-item directory toggle-container" data-name="' . $d . '">' .
				'<span class="toggler">' .
					'<span class="toggle-icon">&#x229e;</span>' .
					'<span class="title">' . $d . '</span></span>';
		makeBrowser($dir . '/' . $d, $basemodule . $d . '.');
		echo '</div>';
	}

	foreach ($elems['modules'] as $m) {
		$fullm = $basemodule . $m;
		echo '<div class="browser-item module" data-name="' . $m . '">' . $m . '</div>';
	}

	echo '</div>';
}

$index = get_index();

echo '<div class="browser">';
foreach ($index as $lib) {
	echo '<div class="browser-item directory toggle-container" data-name="' . $lib['name'] . '">' .
			'<span class="toggler">' .
				'<span class="toggle-icon">&#x229e;</span>' .
				'<span class="title">' . $lib['name'] . '</span>';
	if (isset($lib['url'])) {
		echo '&nbsp;<a class="more-info" href="' . $lib['url'] . '" target="_blank" title="More information" onclick="arguments[0].stopPropagation();">i</a>';
	}
	echo '</span>';
	makeBrowser(CLEANLIB . '/' . $lib['name'] . '/lib', '');
	echo '</div>';
}
echo '</div>';

$all_libs = [];
foreach ($index as $lib) {
	$all_libs[$lib['name']] = [
		'version' => $lib['version'],
		'license' => $lib['license'],
		'url' => $lib['url']
	];

	$nitrile_json = CLEANLIB . '/' . $lib['name'] . '/.nitrile.json';
	if (file_exists($nitrile_json)) {
		$contents = json_decode(file_get_contents($nitrile_json), true);
		if (array_key_exists('git_ref', $contents))
			$all_libs[$lib['name']]['git_ref'] = $contents['git_ref'];
		$all_libs[$lib['name']]['sourcemap'] = $contents['fileinfo'];
	}
}

echo '<script>';
echo 'const indexed_libraries='.json_encode($all_libs).';';
echo '</script>';
