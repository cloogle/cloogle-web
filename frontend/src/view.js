/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

function parseHash(hash) {
	const result = {};

	const hashelems = hash.split(';');
	result.path = hashelems[0];

	const modelems = result.path.split('/');
	result.lib = modelems[0];
	result.mod_path = modelems.slice(1);

	result.icl = false;
	result.lines = null;

	for (var i = 1; i < hashelems.length; i++) {
		if (hashelems[i] == 'icl')
			result.icl = true;
		else if (hashelems[i].substring(0,5) == 'line=')
			result.lines = hashelems[i].substring(5);
	}

	return result;
}

function repoButtonClick(type, which_url) {
	const parsed_hash = parseHash(window.location.hash.substring(1));

	if (!(parsed_hash.lib in indexed_libraries) || !indexed_libraries[parsed_hash.lib].sourcemap) {
		window.alert(type + ' links are not available for this library.');
		return;
	}

	const info = indexed_libraries[parsed_hash.lib];

	var path = parsed_hash.mod_path.join('/');
	path += parsed_hash.icl ? '.icl' : '.dcl';

	if (!('lib/' + path in info.sourcemap)) {
		window.alert(path + ' could not be found in the sourcemap.');
		return;
	}
	path = info.sourcemap['lib/' + path].src;

	if (parsed_hash.lines) {
		if (parsed_hash.lines.indexOf('-') >= 0) {
			const lines = parsed_hash.lines.split('-');
			path += '#L' + lines[0] + '-' + lines[1]; // assume GitLab
		} else {
			path += '#L' + parsed_hash.lines;
		}
	}

	const git_ref = info.git_ref || ('v' + info.version);
	const url = info.url + '/-/' + which_url + '/' + git_ref + '/' + path;

	window.open(url, '_blank');
}

function selectLines(n) {
	var firstline = null;
	browser.state.lines = n;

	var hll = document.getElementsByClassName('hll');
	while (hll.length > 0)
		hll[0].classList.remove('hll', 'hll-first', 'hll-last');

	if (!isNaN(n)) {
		firstline = n;
		var linespan = document.getElementById('line-' + n);
		if (linespan != null)
			linespan.classList.add('hll', 'hll-first', 'hll-last');
	} else if (typeof n == 'string') {
		var match = n.match(/^(\d+)-(\d+)$/);
		if (match != null) {
			firstline = match[1];
			var first = true;
			for (var i = firstline; i < match[2]; i++) {
				var linespan = document.getElementById('line-' + i);
				if (linespan != null) {
					linespan.classList.add('hll');
					if (first) {
						linespan.classList.add('hll-first');
						first = false;
					}
				}
			}
			var linespan = document.getElementById('line-' + match[2]);
			if (linespan != null)
				linespan.classList.add('hll', 'hll-last');
		}
	}

	browser.triggerChange(false);

	return firstline;
}

function selectLinesByEvent(e, n) {
	if (e.shiftKey) {
		if (!isNaN(browser.state.lines)) {
			if (browser.state.lines < n)
				selectLines(browser.state.lines + '-' + n);
			else
				selectLines(n + '-' + browser.state.lines);
			return;
		} else if (typeof n == 'string') {
			var match = n.match(/^(\d+)-(\d+)$/);
			if (match != null) {
				if (n < match[1])
					selectLines(n + '-' + match[2]);
				else
					selectLines(match[1] + '-' + n);
				return;
			}
		}
	}

	selectLines(n);
}

var tableLineNo = null;
function tableHighlightCallback(span, cls, str) {
	var html = '';
	if (tableLineNo == null) {
		html = '<tr id="line-1"><td onclick="selectLinesByEvent(event,1);">1</td><td>';
		tableLineNo = 1;
	}
	var lines = str.split('\n');
	for (var i = 0; i < lines.length-1; i++) {
		tableLineNo++;
		html += highlightCallback(
				'<span class="' + cls + '">' + (lines[i].length ? escapeHTML(lines[i]) : '\n') + '</span>',
				cls, lines[i]) +
			'</td></tr><tr id="line-' + tableLineNo + '"><td onclick="selectLinesByEvent(event,' + tableLineNo + ');">'
				+ tableLineNo + '</td><td>';
	}
	html += highlightCallback(
		'<span class="' + cls + '">' + escapeHTML(lines[lines.length-1]) + '</span>',
		cls, lines[lines.length-1]);
	return html;
}

var browser = null;
window.onload = function() {
	var viewer = document.getElementById('viewer');
	var icl = document.getElementById('icl');

	browser = document.getElementsByClassName('browser')[0].browser({
		newPath: function (path) {
			this.state.mod = path.join('/');
			this.state.lines = null;
			this.newState();
		},
		newHash: function (hash) {
			const parsed = parseHash(hash);

			const update = this.state.mod != parsed.path || this.state.icl != parsed.icl;

			this.state.mod = parsed.path;
			icl.checked = this.state.icl = parsed.icl;
			this.state.lines = parsed.lines;

			if (this.state.lines != null)
				selectLines(this.state.lines);

			browser.openPath(this.state.mod.split('/'));
			browser.triggerChange(update);
		},
		newState: function () {
			var hash = this.state.mod;
			if (this.state.icl)
				hash += ';icl';
			if (this.state.lines != null)
				hash += ';line=' + this.state.lines;

			browser.setHash(hash);
		},
		getUrl: function () {
			var url = 'src.php?mod=' + this.state.mod;
			if (this.state.icl)
				url += '&icl';
			if (this.state.lines != null)
				url += '&line=' + this.state.lines;
			return url;
		},
		viewer: viewer,
		onLoad: function(state, text) {
			tableLineNo = null;

			var library = indexed_libraries[state.mod.split('/')[0]];
			var license_link = '<a target="_blank" href="' + library.url + '">This library</a>';
			if ('license' in library && library.license !== null)
				license_link += ' is licensed under ' +
					'<a target="_blank" href="https://spdx.org/licenses/' + library.license + '.html">' +
					library.license + '</a>.';
			else
				license_link += ' is <em>unlicensed. All rights reserved.</em>';

			viewer.innerHTML = '<table class="source-code">' +
					'<tr><td></td><td class="license">' + license_link + '</td></tr>' +
					highlightClean(text, tableHighlightCallback) +
				'</table>';
			viewer.scrollLeft = 0;
			if (state.lines != null) {
				var firstline = selectLines(state.lines);
				if (firstline != null)
					browser.scrollTo(document.getElementById('line-' + firstline));
			} else {
				browser.scrollTo();
			}

			var modparts = state.mod.split('/');
			if (modparts.length > 1) {
				var lib = modparts.shift();
				document.title = modparts.join('.') + ' (' + lib + ') - Cloogle library browser';
			}
		},
		state: {
			icl: false
		}
	});

	browser.state.icl = icl.checked;
	icl.onchange = function() {
		browser.state.icl = this.checked;
		browser.state.lines = null;
		browser.triggerChange();
	};

	browser.open();
};
