<!DOCTYPE html>
<!--
 - Copyright 2016-2022 the authors (see README.md).
 -
 - This file is part of cloogle-web.
 -
 - Cloogle-web is free software: you can redistribute it and/or modify it under
 - the terms of the GNU Affero General Public License as published by the Free
 - Software Foundation, version 3 of the License.
 -
 - Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 - ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 - FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 - for more details.
 -
 - You should have received a copy of the GNU Affero General Public License
 - along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 -
 - The software is licensed under additional terms under section 7 of the GNU
 - Affero General Public License; see the LICENSE file for details.
-->
<?php require_once('conf.php'); ?>
<html lang="en">
<head>
	<!-- cloogle/cloogle-web {{{COMMIT}}} -->
	<title>Cloogle</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Cloogle is a search engine for Clean"/>
	<meta name="keywords" content="Clean,Clean language,Concurrent Clean,nitrile,search,functions,search engine,programming language,iTasks,cloogle,hoogle"/>
	<script src="clean-highlighter/clean.js" defer="defer"></script>
	<script src="clean-doc-markup/clean-doc-markup.js" defer="defer"></script>
	<script src="common.js" defer="defer"></script>
	<script src="api.js" defer="defer"></script>
	<link rel="stylesheet" href="common.css" type="text/css"/>
	<link rel="stylesheet" href="clean-highlighter/clean.css" type="text/css"/>
	<link rel="stylesheet" href="frontend.css" type="text/css"/>
</head>
<body>
	<?php include('banners.php'); ?>
	<div id="header">
		<div id="logo">
			<a href="https://gitlab.com/cloogle/cloogle-web">
				<img src="logo.png" alt="follow link for the sourcecode" />
			</a>
		</div>
		<div id="search">
			<form id="search-form" action="#">
				<input id="search-str" spellcheck="false" autocapitalize="none" autocomplete="off"/>
				<button>Search</button><br/>
				<ul id="searchlinks">
					<li id="link-advanced" onclick="toggleTooltip('advanced')">Advanced</li>
					<li class="separator">|</li>
					<li id="link-helptext" onclick="toggleTooltip('helptext');">How to use</li>
					<li class="separator">|</li>
					<li id="link-contributetext" onclick="toggleTooltip('contributetext');">Contribute</li>
					<li class="separator">|</li>
					<li id="link-legaltext" onclick="toggleTooltip('legaltext');">Terms &amp; Privacy</li>
				</ul>
			</form>
		</div>
		<div id="advanced" class="tooltip">
			<div>
				<label><input type="checkbox" id="include-builtins" checked="checked"/> Include language builtins</label><br/>
				<label><input type="checkbox" id="include-core"/> Include library core modules</label><br/>
				<div id="libraries">
					<p>Enabled libraries (<a href="javascript:toggleLibraries(true);">all</a> / <a href="javascript:toggleLibraries(false);">none</a>)</p>
					<div id="library-list">
						<?php
							function make_group_id($name) {
								return 'libs-' . str_replace(' ', '-', $name);
							}

							$index = get_index();
							foreach ($index as $lib) {
								echo '<label title="' . $lib['description'] . '">';
								echo '<input type="checkbox" class="search-libs" checked="checked" value="' . $lib['name'] . '"/>';
								echo ' ' . $lib['name'];
								if (isset($lib['url'])) {
									echo ' <a class="more-info" href="' . $lib['url'] . '" target="_blank" title="More information">i</a>';
								}
								echo '</label><br/>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div id="helptext" class="tooltip">
			<div>
				<p>Cloogle is a search engine for <a href="https://clean-lang.org">Clean</a>. It indexes the entire <a href="https://clean-and-itasks.gitlab.io/nitrile/">Nitrile</a> registry.</p>
				<p>The following search strings are recognised:</p>
				<table id="search-examples">
					<tr>
						<td class='code'>hd</td>
						<td class='description'>The symbol name <code>hd</code>, or documentation containing <code>hd</code></td>
					</tr>
					<tr>
						<td class='code'>:: a [a] -&gt; a</td>
						<td class='description'>Functions with a type unifiable with <code>a [a] -&gt; a</code></td>
					</tr>
					<tr>
						<td class='code'>hd :: [a] -&gt; a</td>
						<td class='description'>A combination of the above</td>
					</tr>
					<tr>
						<td class='code'>:: A.a: [a] -&gt; a</td>
						<td class='description'>Type search, where <code>a</code> cannot be unified</td>
					</tr>
					<tr>
						<td class='code'>\\</td>
						<td class='description'>Information about the syntax construct <code>\\</code></td>
					</tr>
					<tr>
						<td class='code'>stack overflow</td>
						<td class='description'>Information about the error message "stack overflow"</td>
					</tr>
					<tr>
						<td class='code'>using Int; ==</td>
						<td class='description'>Anything that uses <code>Int</code> <em>and</em> <code>==</code></td>
					</tr>
					<tr>
						<td class='code'>exact Text</td>
						<td class='description'>Anything with the exact name <code>Text</code></td>
					</tr>
					<tr>
						<td class='code'>class Text</td>
						<td class='description'>Classes with the exact name <code>Text</code></td>
					</tr>
					<tr>
						<td class='code'>type Int</td>
						<td class='description'>Types with the exact name <code>Int</code></td>
					</tr>
				</table>
				<p>You can also <a href="src">browse the index</a> and <a href="doc">read the documentation</a>.</p>
				<p>There are also <a href="https://gitlab.com/cloogle/cloogle-web/#interfacing-with-cloogle">programmatic interfaces to Cloogle</a>.</p>
			</div>
		</div>
		<div id="contributetext" class="tooltip">
			<div>
				<p>
					Development takes place on <a href="https://gitlab.com/cloogle/cloogle-web">GitLab</a>.
					For bug reports, please open an issue in the <a href="https://gitlab.com/cloogle/cloogle-web/issues">issue tracker</a>.
				</p>
			</div>
		</div>
		<div id="legaltext" class="tooltip">
			<div>
				<h3>Copyright</h3>
				<p>Cloogle-web&ensp;Copyright &copy; 2016&ndash;<?=date('Y')?>&ensp;Camil Staps and contributors.</p>
				<h3>License</h3>
				<p>Cloogle-web is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.</p>
				<p>Cloogle-web is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.</p>
				<p>You should have received a copy of the GNU Affero General Public License along with cloogle-web. If not, see <a href="https://www.gnu.org/licenses/" target="_blank">https://www.gnu.org/licenses/</a>.</p>
				<p>The software is licensed under additional terms under section 7 of the GNU Affero General Public License; see the LICENSE file for details.</p>
				<p>For more details, and to download the source code, see <a href="https://gitlab.com/cloogle/cloogle-web">GitLab</a>.</p>
				<h3>Logo license</h3>
				<p>The logo was created by Erin van der Veen and is licensed under a <a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>.</p>
				<h3>Privacy policy</h3>
				<p>Cloogle-web keeps a log linking queries to a date, user agent, and IP address, as required for <a href="stats">our statistics</a>.</p>
				<p>The IP address is stored with the date and user agent in a non-invertible hash and cannot be traced to you, your device, or your physical location.</p>
				<p>In addition, the Cloogle web server keeps a log in which IP addresses are stored in plain text format for up to 3 years.</p>
			</div>
		</div>
	</div>
	<div id="search-results"></div>
</body>
</html>
