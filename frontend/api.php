<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

error_reporting(0);

header('Content-Type: application/json');

define('SERVER_HOSTNAME', $_ENV['CLOOGLE_BACKEND_HOSTNAME']);
define('SERVER_PORT', 31215);
define('SERVER_TIMEOUT', 3);

if (file_exists('conf.php'))
	require_once('conf.php');

$start_time = microtime(true);

$db = new mysqli(
	CLOOGLE_DB_HOST, CLOOGLE_DB_USER, CLOOGLE_DB_PASS, CLOOGLE_DB_NAME);
if (mysqli_connect_errno()) {
	$db = null;
} else {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$ua_hash = md5($ua);

	$stmt = $db->prepare('SELECT `id` FROM `useragent` WHERE `ua_hash`=?');
	$stmt->bind_param('s', $ua_hash);
	$stmt->execute();
	$stmt->bind_result($ua_id);
	if ($stmt->fetch() !== true) {
		$stmt->close();
		$stmt = $db->prepare(
			'INSERT INTO `useragent` (`useragent`,`ua_hash`) VALUES (?,?)');
		$stmt->bind_param('ss', $ua, $ua_hash);
		$stmt->execute();
		$ua_id = $stmt->insert_id;
	}
	$stmt->close();

	/* Take the hash of the date, the IP, and the user agent (hash). This is
	 * enough to track unique visitors, but does not allow tracking users over
	 * multiple days, or to link requests from devices on the same network. */
	$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ?
		$_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	$ip_hash = date('dmy') . $ip . $ua_hash;
	$ip_hash = substr(md5($ip_hash), 0, 12);
}

function log_request($code) {
	global $db, $ua_id, $ip_hash;

	if (is_null($db))
		return;

	global $start_time;
	$time = (int) ((microtime(true) - $start_time) * 1000);

	$stmt = $db->prepare('INSERT INTO `log`
		(`ip`,`useragent_id`,`query`,`responsecode`,`responsetime`)
		VALUES (?,?,?,?,?)');
	$str = substr($_GET['str'], 0, 199);
	$stmt->bind_param('sisii', $ip_hash, $ua_id, $str, $code, $time);
	$stmt->execute();
	$stmt->close();

	$db->close();
}

function error($code, $msg) {
	log_request($code);

	echo json_encode([
		'return' => $code,
		'data' => [],
		'msg' => $msg
	]);
}

if ($_SERVER['REQUEST_METHOD'] !== 'GET'){
	error(E_ILLEGALMETHOD, 'Can only be accessed by GET request');
} else if (!isset($_GET['str'])){
	error(E_ILLEGALREQUEST, 'GET variable "str" must be set');
} else if (strlen($_GET['str']) >= 400) {
	error(E_QUERYTOOLONG, 'Query too long');
} else {
	$str = array_map('trim', explode('::', $_GET['str']));
	$name = $str[0];
	$unify = '';
	if (isset($str[1])) {
		$unify = trim($str[1]);
		if ($unify == '')
			$name .= '::';
	}
	$name = trim($name);
	$command = [];

	if ($unify != '') {
		$command['unify'] = $unify;
	}

	if (substr($name, 0, 6) == 'using ') {
		$command['using'] = [];
		$usingnames = explode(';', substr($name, 6));
		foreach ($usingnames as $usingname) {
			$command['using'][] = trim($usingname);
		}
	} elseif (substr($name, 0, 6) == 'class ') {
		$command['className'] = substr($name, 6);
	} elseif (substr($name, 0, 9) == 'instance ') {
		$command['className'] = substr($name, 9);
	} elseif (substr($name, 0, 5) == 'type ') {
		$command['typeName'] = substr($name, 5);
	} elseif (substr($name, 0, 6) == 'exact ') {
		$command['exactName'] = substr($name, 6);
	} elseif ($name != '') {
		$command['name'] = $name;
	}

	if (isset($_GET['lib'])) {
		$command['libraries'] = explode(',', $_GET['lib']);
	}

	if (isset($_GET['include_builtins'])) {
		$command['include_builtins'] = $_GET['include_builtins'] == 'true';
	}

	if (isset($_GET['include_core'])) {
		$command['include_core'] = $_GET['include_core'] == 'true';
	}

	if (isset($_GET['mod'])) {
		$command['modules'] = explode(',', $_GET['mod']);
	}

	if (isset($_GET['page'])) {
		$command['page'] = (int) $_GET['page'];
	}

	$skt = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if (!socket_connect($skt, SERVER_HOSTNAME, SERVER_PORT)) {
		error(E_CLOOGLEDOWN, 'Cloogle server unreachable');
	} else {
		$response = '';
		socket_write($skt, json_encode($command));
		$read = [$skt];
		$write = NULL;
		$except = NULL;
		if (socket_select($read, $write, $except, SERVER_TIMEOUT) !== 1) {
			error(E_TIMEOUT, 'Connection to the Cloogle server timed out');
		} else {
			while (($_response = socket_read($skt, 128, PHP_NORMAL_READ)) !== false) {
				$response .= $_response;
				if (strpos($_response, "\n") !== false)
					break;
			}
			$decoded = json_decode($response, true);
			if ($decoded === null) {
				error(E_SERVERERROR, 'Received invalid JSON from the Cloogle server');
			} else {
				log_request($decoded['return']);
				echo $response;
			}
		}
		socket_close($skt);
	}
}
