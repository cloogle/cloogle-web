/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

CREATE TABLE IF NOT EXISTS `log` (
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`ip` varchar(12) NOT NULL,
	`useragent_id` int(10) unsigned NOT NULL,
	`query` varchar(400) NOT NULL,
	`responsecode` tinyint(4) unsigned NOT NULL,
	`responsetime` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `useragent` (
	`id` int(10) unsigned NOT NULL,
	`useragent` text,
	`ua_hash` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `log`
	ADD KEY `useragent_id` (`useragent_id`);

ALTER TABLE `useragent`
	ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ua_hash` (`ua_hash`);
ALTER TABLE `useragent`
	MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `log`
	ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`useragent_id`) REFERENCES `useragent` (`id`);
