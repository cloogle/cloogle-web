#!/bin/bash

# Fetch libraries according to index.json.
#
# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

set -e
set -o pipefail

shopt -s globstar

DEST="$1"
THIS_DIR="$(dirname "${BASH_SOURCE[0]}")"

rm -rf "$DEST"
mkdir -p "$DEST"

# Used for processing packages not published on clean-lang.org.
EXTRA_PACKAGES_FILE="$THIS_DIR"/extra_packages.json

PACKAGES="$(curl --retry 5 -# https://clean-lang.org/api/packages)"

if [ -f "$EXTRA_PACKAGES_FILE" ]; then
	PACKAGES="$(jq -s '.[0] + .[1]' "$EXTRA_PACKAGES_FILE" <(echo "$PACKAGES"))"
fi

echo "$PACKAGES" | jq -rf "$THIS_DIR"/fetch_libs.jq | jq -sc 'del(.[].deploy_token)' > "$DEST"/index.json

exec 5< <(echo "$PACKAGES" | jq -rf "$THIS_DIR"/fetch_libs.jq | jq -r '.name, .version, .download, .deploy_token')

while read lib <&5
do
	read version <&5
	read url <&5
	read deploy_token <&5

	rm -rf "$DEST/$lib"
	mkdir "$DEST/$lib"

	echo "Fetching $lib@$version: $url..."
	curl --retry 5 -# -L ${deploy_token:+ --header "DEPLOY-TOKEN: ${deploy_token}"} "$url" | tar xz --strip-components=1 -C "$DEST/$lib"

	ls "$DEST/$lib"/lib/**/*.dcl >/dev/null 2>/dev/null || (
		echo "Deleting $lib: no definition modules in lib directory"
		rm -rf "$DEST/$lib"
	)
done

echo "Converting to UTF-8..."
for f in "$DEST"/**/*.[id]cl; do
	enc="$(file -bi "$f" | grep -Po '(?<=charset=).*')"
	if [ "$enc" != 'us-ascii' -a "$enc" != 'binary' -a "$enc" != 'utf-8' ]; then
		iconv -f "$enc" -t utf-8 < "$f" > "$f.tmp"
		mv "$f.tmp" "$f"
		echo "Converted $f from $enc to utf-8"
	fi
done

echo "All done."
