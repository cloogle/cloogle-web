#!/bin/bash

# Update a cloogle-web instance.
#
# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

set -e

TEMP_LOG="/tmp/cloogle-build.log"

CLEAR_CACHE=-1
INTERACTIVE=1
MAIL_FAILURE=()

# This script is for production. It is assumed that:
# - you want to clean the cache periodically, so we use --with-garbage-collection
# - the server has another MySQL server which is used, so we don't use with-statistics
PROFILES="--profile with-garbage-collection"

escape_sed() {
	sed -e 's/\//\\\//g' -e 's/\&/\\\&/g'
}

while [ $# -gt 0 ]; do
	case "$1" in
		--interactive ) INTERACTIVE=1;;
		--no-interactive ) INTERACTIVE=0;;
		--clear-cache ) CLEAR_CACHE=1;;
		--no-clear-cache ) CLEAR_CACHE=0;;
		--huginn ) HUGINN="$2"; shift;;
		--release-directory ) RELEASE_DIRECTORY="$2"; shift;;
		--mail-failure ) MAIL_FAILURE+=("$2"); shift;;
		-h|--help ) cat <<EOF
Usage: $0 [options]

This tool can be used to update a Cloogle instance. It does the following:

 1. Pull new commits from upstream.
 2. Rebuild docker images.
 3. Recreate docker containers.
 4. Optionally clear the query cache (--[no-]clear-cache).
 5. Optionally notify a huginn instance about success/failure (--huginn).
 6. Optionally publish build artifacts (--release-directory).

Options:

  -h, --help
      Show this help text.

  --[no-]interactive
      With --no-interactive, no editor will be opened when the git pull causes
      a conflict. Default: --interactive. --no-interactive should be used in
      automated scripts.

  --[no-]clear-cache
      With --clear-cache, the query cache is cleared. This should be used when
      the update causes significant changes to the index and/or search logic,
      which would invalidate previous results.

  --huginn ADDRESS
      Make a POST request to ADDRESS with a descriptive message ("Cloogle build
      succeeded/failed") in the variable 'text'. This can be used to track
      automatic updates with a Huginn instance.

  --release-directory DIR
      Publish build artifacts to DIR. A new directory is created for the
      current date, in which the built index and other artifacts are stored. A
      symbolic link 'latest' is created in DIR to point to the new directory.
      This is useful to maintain a historical resource for future use.

  --mail-failure EMAIL
      Send an email to EMAIL when the build fails. Requires GNU mailutils. This
      option can be given multiple times.
EOF
			exit;;
		* )
			echo "Unknown argument '$1'; use -h for help"
			exit -1;;
	esac
	shift
done

exit_trap () {
	EXIT_CODE=$?

	# Reset any ANSII color codes
	echo -en "\033[0m"

	if [[ $EXIT_CODE -ne 0 ]]; then
		echo "--> Cloogle build failed."

		# Send mails
		for ADDR in "${MAIL_FAILURE[@]}"; do
			cat "$TEMP_LOG" |& mail.mailutils -s "Cloogle build failure" "$ADDR"
		done

		# Notify huginn
		if [[ "$HUGINN" != "" ]]; then
			curl -s -d text="Cloogle build failed." "$HUGINN"; echo
		fi
	else
		echo "--> Cloogle build succeeded."

		# Notify huginn
		if [[ "$HUGINN" != "" ]]; then
			curl -s -d text="Cloogle build succeeded." "$HUGINN"; echo
		fi
	fi

	rm -f "$TEMP_LOG"

	exit $EXIT_CODE
}

trap "exit_trap" EXIT

# Redirect all output of this script to TEMP_LOG, so that --mail-failure gets a nice log.
rm -f "$TEMP_LOG"
exec &> >(tee "$TEMP_LOG")

if [[ $INTERACTIVE -eq 0 ]] && [[ $CLEAR_CACHE -lt 0 ]]; then
	echo "When using --no-interactive you must use either --no-clear-cache or --clear-cache."
	exit -1
fi

echo "Pulling new commits..."

git checkout frontend/index.php
if [ $INTERACTIVE -eq 0 ]; then
	git pull origin main
else
	git pull --no-edit origin main
fi

COMMIT_INFO="$(git log -1 --decorate --pretty=oneline --no-color | escape_sed)"
sed -i "s/{{{COMMIT}}}/$COMMIT_INFO/g" "frontend/index.php"

echo "Updating containers..."

sudo stdbuf -o0 -e0 docker-compose $PROFILES build --force-rm --no-cache --pull
sudo docker-compose $PROFILES up -d
sudo docker image prune -f

echo "All done."

if [ $CLEAR_CACHE -lt 0 ]; then
	echo
	read -p "Do you want to clear the caches? (y/[n]) " confirm
	case "$confirm" in
		y|Y ) CLEAR_CACHE=1;;
		* ) CLEAR_CACHE=0;;
	esac
fi

if [ $CLEAR_CACHE -eq 1 ]; then
	echo "Clearing the cache..."
	sudo bash -c 'rm -f cache/*/*'
else
	echo "Not clearing the cache."
fi

if [[ "$RELEASE_DIRECTORY" != "" ]]; then
	DATE="$(date +%Y-%m-%d)"
	THIS_RELEASE="$RELEASE_DIRECTORY/~$DATE"
	mkdir -p "$THIS_RELEASE"
	rm -f "$RELEASE_DIRECTORY/latest"
	ln -s "~$DATE" "$RELEASE_DIRECTORY/latest"
	sed 's/\x1b\[[0-9;]*m//g' /tmp/cloogle-build.log \
		| sed -n '/^\.\/builddb /{:a;n;/^Execution: /{p;b};p;ba}' \
		> "$THIS_RELEASE/build-log.txt"
	timeout -k 10 10 sudo docker-compose exec -T backend cat rank_settings.json > "$THIS_RELEASE/rank_settings.json" || echo "could not publish rank settings"
	timeout -k 10 10 sudo docker-compose exec -T backend cat db.jsonl > "$THIS_RELEASE/db.jsonl" || echo "could not publish database"
	timeout -k 10 10 sudo docker-compose exec -T backend cat typetree.dot > "$THIS_RELEASE/typetree.dot" || echo "could not publish type tree"
fi
