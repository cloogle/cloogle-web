# Combine multiple index files into a single one.
#
# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

# Filter out deprecated packages
map(select(.deprecated != true))
# Iterate over all packages
[]
# For convenience, define the $targets
| .versions[.latest_version].targets as $tgts
# Find the best target, or pick one at random
| ["any-any", "any-x64", "linux-any", "linux-x64", "windows-any", "windows-x64"] as $goals
| ($goals | map($tgts[.]) | map(select(type != "null")))[0] as $tgt
| (if $tgt == null then $tgts | to_entries | .[0].value else $tgt end).url as $url
# Output only the information we need
| {name: .name, description: .description, url: .url, license: .license, version: .latest_version, download: $url, deploy_token: .deploy_token}
