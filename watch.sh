#!/bin/bash

unset LOCAL_CLOOGLE
unset LOCAL_LIBCLOOGLE

BUILD_INDEXER=yes
INDEXER=yes

BUILD_SERVER=yes
RANK_SETTINGS=yes
SERVER=yes

unset TEST_LOG

while [ $# -gt 0 ]; do
	case "$1" in
		--local-cloogle)
			LOCAL_CLOOGLE=yes
			;;
		--local-libcloogle)
			LOCAL_LIBCLOOGLE=yes
			;;
		--no-indexer)
			unset BUILD_INDEXER
			unset INDEXER
			;;
		--no-rank-settings)
			unset RANK_SETTINGS
			;;
		--no-server)
			unset SERVER
			;;
		--test)
			TEST_LOG="$2"
			shift
			;;
		*)
			echo "Unknown option '$var'"
			exit -1
	esac

	shift
	if [ $# -le 0 ]; then
		break
	fi
done

if [ -z ${RANK_SETTINGS+x}${SERVER+x} ]; then
	unset BUILD_SERVER
fi

set -x

nitrile \
	${LOCAL_CLOOGLE+--local-dependency cloogle ../cloogle} \
	${LOCAL_LIBCLOOGLE+--local-dependency libcloogle ../libcloogle} \
	watch \
	${BUILD_INDEXER+--do build} \
	${INDEXER+--do shell -c "cd backend; ./builddb --no-license $BUILDDB_ARGS > db.jsonl"} \
	${BUILD_SERVER+--do build --arch=x86} \
	${RANK_SETTINGS+--do shell -c 'cd backend; ./CloogleServer --no-license --rank-settings-constraints rank_settings_constraints.json | tee rank_settings.json'} \
	${TEST_LOG+--do shell -c "cd backend; ./CloogleServer -t --no-license --test $TEST_LOG"} \
	${SERVER+--do shell -c 'cd backend; ./CloogleServer --no-license'}
